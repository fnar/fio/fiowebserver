using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace FIOWebServer.Models
{
    public class ShipmentFinderModel
    {
        public string PlanetId { get; set; }
        public string PlanetNaturalId { get; set; }
        public string PlanetName { get; set; }
        public string PlanetDisplayName
        {
            get
            {
                return (PlanetNaturalId != PlanetName) ? $"{PlanetName} ({PlanetNaturalId})" : PlanetNaturalId;
            }
        }

        public string ContractNaturalId { get; set; }

        public string OriginPlanetId { get; set; }
        public string OriginPlanetNaturalId { get; set; }
        public string OriginPlanetName { get; set; }
        public string OriginPlanetDisplayName
        {
            get
            {
                return (OriginPlanetNaturalId != OriginPlanetName) ? $"{OriginPlanetName} ({OriginPlanetNaturalId})" : OriginPlanetNaturalId;
            }
        }

        public string DestinationPlanetId { get; set; }
        public string DestinationPlanetNaturalId { get; set; }
        public string DestinationPlanetName { get; set; }
        public string DestinationPlanetDisplayName
        {
            get
            {
                return (DestinationPlanetNaturalId != DestinationPlanetName) ? $"{DestinationPlanetName} ({DestinationPlanetNaturalId})" : DestinationPlanetNaturalId;
            }
        }

        public double CargoWeight { get; set; }
        public string CargoWeightDisplay
        {
            get
            {
                return CargoWeight.ToString("N2") + "t";
            }
        }

        public double CargoVolume { get; set; }
        public string CargoVolumeDisplay
        {
            get
            {
                return CargoVolume.ToString("N2") + "m&sup3;";
            }
        }

        public string CreatorCompanyId { get; set; }
        public string CreatorCompanyName { get; set; }
        public string CreatorCompanyCode { get; set; }

        public double PayoutPrice { get; set; }
        public string PayoutCurrency { get; set; }
        public string PayoutDisplay
        {
            get
            {
                return PayoutPrice.ToString("N2") + $" {PayoutCurrency}";
            }
        }

        public double PayoutPricePer500Ton
        {
            get
            {
                return 500.0 / CargoWeight * PayoutPrice;
            }
        }
        public double PayoutPricePer500MeterSquared
        {
            get
            {
                return 500.0 / CargoVolume * PayoutPrice;
            }
        }
        public double PayoutPricePer500
        {
            get
            {
                return (CargoWeight >= CargoVolume) ? PayoutPricePer500Ton : PayoutPricePer500MeterSquared;
            }
        }
        public string PayoutPricePer500UnitsDisplay
        {
            get
            {
                if (CargoWeight >= CargoVolume)
                {
                    string ppt = PayoutPricePer500Ton.ToString("N2");
                    return $"{ppt} {PayoutCurrency}/500t";
                }
                else
                {
                    string ppv = PayoutPricePer500MeterSquared.ToString("N2");
                    return $"{ppv} {PayoutCurrency}/500m&sup3;";
                }
            }
        }

        public int DeliveryTime { get; set; }
        public string DeliveryTimeDisplay
        {
            get
            {
                string suffix = DeliveryTime > 1 ? "s" : "";
                return $"{DeliveryTime} day{suffix}";
            }
        }

        public long CreationTimeEpochMs { get; set; }
        public long ExpiryTimeEpochMs { get; set; }

        public string MinimumRating { get; set; }
        public string RatingDisplay
        {
            get
            {
                if (MinimumRating == "PENDING")
                {
                    return "P";
                }
                else
                {
                    return MinimumRating;
                }
            }
        }

        public bool SourceIsGaseous { get; set; }
        public bool DestinationIsGaseous { get; set; }

        public int JumpDistance { get; set; }

        public enum ShipmentFinderSearchType
        {
            Origin,
            Destination,
            All
        }

        public static async Task<List<ShipmentFinderModel>> GetShipmentFinderModels(string PlanetInput, ShipmentFinderSearchType SearchType)
        {
            if (!string.IsNullOrWhiteSpace(PlanetInput))
            {
                string Endpoint = null;
                switch(SearchType)
                {
                    case ShipmentFinderSearchType.Origin:
                        Endpoint = $"/localmarket/shipping/source/{PlanetInput}";
                        break;
                    case ShipmentFinderSearchType.Destination:
                        Endpoint = $"/localmarket/shipping/destination/{PlanetInput}";
                        break;
                    case ShipmentFinderSearchType.All:
                        Endpoint = $"/localmarket/shipping/all/{PlanetInput}";
                        break;
                }

                var shipmentFinderRequest = new Web.Request(HttpMethod.Get, Endpoint);
                var result = await shipmentFinderRequest.GetResponseAsync<List<ShipmentFinderModel>>();
                if ( result != null )
                {
                    return result;
                }
            }

            // In the failure case, return an empty list
            return new List<ShipmentFinderModel>(); 
        }
    }
}
