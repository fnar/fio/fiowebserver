using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using FIOWebServer.JsonPayloads;

using Newtonsoft.Json;

namespace FIOWebServer.Models
{
    public class PlanetResource
    {
        public bool IsValid { get; set; } = false;

        public string Material { get; set; }

        [DisplayName("Type")]
        public string ResourceType { get; set; }

        public float Concentration { get; set; }

        [DisplayName("Daily Extraction")]
        public double DailyExtraction { get; set; }
    }

    public class PlanetSearchModel
    {
        public string PlanetNaturalId { get; set; }
        public string PlanetName { get; set; }

        [DisplayName("Planet")]
        public string DisplayName { get; set; }

        public bool IsFertile { get; set; }
        public float Fertility { get; set; }

        [DisplayName("Type")]
        public string SurfaceType { get; set; }

        public string Gravity { get; set; }
        public string Pressure { get; set; }
        public string Temperature { get; set; }

        public string FactionCode { get; set; }

        [DisplayName("LM")]
        public bool HasLocalMarket { get; set; }

        public bool HasChamberOfCommerce { get; set; }

        [DisplayName("WAR")]
        public bool HasWarehouse { get; set; }

        [DisplayName("ADM")]
        public bool HasAdministrationCenter { get; set; }

        [DisplayName("SHY")]
        public bool HasShipyard { get; set; }

        [DisplayName("Materials")]
        public string MaterialSummary { get; set; }

        public List<PlanetResource> Resources { get; set; } = new List<PlanetResource>();

        [DisplayName("COGC")]
        public string ActiveCOGCDisplay { get; set; }

        public string ActiveCOGC { get; set; }

        public string CurrencyCode { get; set; }

        public bool HasJobData { get; set; }

        public int? TotalPioneers { get; set; }
        public int? OpenPioneers { get; set; }

        public int? TotalSettlers { get; set; }
        public int? OpenSettlers { get; set; }

        public int? TotalTechnicians { get; set; }
        public int? OpenTechnicians { get; set; }

        public int? TotalEngineers { get; set; }
        public int? OpenEngineers { get; set; }

        public int? TotalScientists { get; set; }
        public int? OpenScientists { get; set; }

        public int? TotalPlots { get; set; }

        public List<int> DistanceResults { get; set; } = new List<int>();

        public static async Task<List<PlanetSearchModel>> GetAllPlanets(List<MaterialPayload> AllJsonPlanetMaterials, List<string> ExchangeLocationNames)
        {
            var psp = new PlanetSearchPayload
            {
                IncludeRocky = true,
                IncludeGaseous = true,
                IncludeLowGravity = true,
                IncludeHighGravity = true,
                IncludeLowPressure = true,
                IncludeHighPressure = true,
                IncludeLowTemperature = true,
                IncludeHighTemperature = true,
                DistanceChecks = ExchangeLocationNames
            };

            return await Search(AllJsonPlanetMaterials, psp);
        }

        public static async Task<List<PlanetSearchModel>> Search(List<MaterialPayload> AllJsonPlanetMaterials, PlanetSearchPayload psp)
        {
            var PlanetSearchModels = new List<PlanetSearchModel>();

            var planetSearchRequest = new Web.Request(HttpMethod.Post, "/planet/search", null, JsonConvert.SerializeObject(psp));
            var searchedPlanetData = await planetSearchRequest.GetResponseAsync<List<PlanetDataPayloadEx>>();
            if (searchedPlanetData != null)
            {
                foreach (var planetData in searchedPlanetData)
                {
                    PlanetSearchModel model = new PlanetSearchModel();

                    model.PlanetNaturalId = planetData.PlanetNaturalId;
                    model.PlanetName = planetData.PlanetName;
                    model.DisplayName = (model.PlanetName != model.PlanetNaturalId) ? $"{model.PlanetName} ({model.PlanetNaturalId})" : model.PlanetNaturalId;

                    model.IsFertile = planetData.Fertility > -1.0;
                    if (model.IsFertile)
                    {
                        model.Fertility = (float)(planetData.Fertility / 3.3);
                    }

                    model.SurfaceType = planetData.Surface ? "Rocky" : "Gaseous";
                    model.Gravity = GetDescriptorStr(planetData.Gravity, 0.25, 2.5);
                    model.Pressure = GetDescriptorStr(planetData.Pressure, 0.25, 2.0);
                    model.Temperature = GetDescriptorStr(planetData.Temperature, -25.0, 75.0);
                    model.HasLocalMarket = planetData.HasLocalMarket;
                    model.HasChamberOfCommerce = planetData.HasChamberOfCommerce;
                    model.HasWarehouse = planetData.HasWarehouse;
                    model.HasAdministrationCenter = planetData.HasAdministrationCenter;
                    model.HasShipyard = planetData.HasShipyard;

                    model.MaterialSummary = String.Empty;
                    foreach (var resource in planetData.Resources)
                    {
                        PlanetResource pr = new PlanetResource();
                        string MatTicker = AllJsonPlanetMaterials.Where(r => r.MaterialId == resource.MaterialId).Select(r => r.Ticker).FirstOrDefault();
                        pr.IsValid = true;
                        pr.Material = MatTicker;
                        pr.ResourceType = resource.ResourceType.Substring(0, 1) + resource.ResourceType.Substring(1).ToLower(); // GASEOUS -> Gaseous
                        pr.Concentration = (float)resource.Factor;
                        pr.DailyExtraction = (resource.ResourceType == "GASEOUS") ? resource.Factor * 60.0 : resource.Factor * 70.0;
                        model.Resources.Add(pr);

                        model.MaterialSummary += $"{pr.Material} ({pr.Concentration:P2}), ";
                    }

                    if (model.MaterialSummary.Length > 2)
                    {
                        model.MaterialSummary = model.MaterialSummary.Substring(0, model.MaterialSummary.Length - 2); // Trim off ", " from the end
                    }

                    model.FactionCode = (planetData.FactionCode != null) ? planetData.FactionCode : "";
                    model.CurrencyCode = (planetData.CurrencyCode != null) ? planetData.CurrencyCode : "";

                    model.HasJobData = planetData.HasJobData;

                    model.TotalPioneers = planetData.TotalPioneers;
                    model.OpenPioneers = planetData.OpenPioneers;
                    model.TotalSettlers = planetData.TotalSettlers;
                    model.OpenSettlers = planetData.OpenSettlers;
                    model.TotalTechnicians = planetData.TotalTechnicians;
                    model.OpenTechnicians = planetData.OpenTechnicians;
                    model.TotalEngineers = planetData.TotalEngineers;
                    model.OpenEngineers = planetData.OpenEngineers;
                    model.TotalScientists = planetData.TotalScientists;
                    model.OpenScientists = planetData.OpenScientists;
                    model.TotalPlots = planetData.TotalPlots;
                    model.DistanceResults = planetData.DistanceResults;

                    long CurrentEpochMs = Utils.GetCurrentEpochMs();
                    foreach (var cogcProgram in planetData.COGCPrograms)
                    {
                        if (CurrentEpochMs >= cogcProgram.StartEpochMs && CurrentEpochMs <= cogcProgram.EndEpochMs)
                        {
                            if (planetData.COGCProgramStatus == "ACTIVE")
                            {
                                model.ActiveCOGC = cogcProgram.ProgramType;

                                // Also handle the case where no program is running
                                if (model.ActiveCOGC == null)
                                {
                                    model.ActiveCOGC = "Inactive";
                                }
                            }
                            else
                            {
                                model.ActiveCOGC = "Inactive";
                            }
                            break;
                        }
                    }

                    if (!(bool)model.HasChamberOfCommerce)
                    {
                        model.ActiveCOGC = "Not Present";
                    }
                    else if ((bool)model.HasChamberOfCommerce && model.ActiveCOGC == null)
                    {
                        model.ActiveCOGC = "No Data";
                    }

                    model.ActiveCOGCDisplay = GetFriendlyCOGCDisplay(model.ActiveCOGC);

                    PlanetSearchModels.Add(model);
                }
            }

            return PlanetSearchModels;
        }

        private static string GetDescriptorStr(double value, double lower, double upper)
        {
            if (value < lower)
            {
                return "Low";
            }
            else if (value > upper)
            {
                return "High";
            }

            return "Normal";
        }

        private static string GetFriendlyCOGCDisplay(string cogc)
        {
            if (!String.IsNullOrWhiteSpace(cogc))
            {
                switch (cogc)
                {
                    case "WORKFORCE_PIONEERS":
                        return "Pioneers";
                    case "WORKFORCE_SETTLERS":
                        return "Settlers";
                    case "WORKFORCE_TECHNICIANS":
                        return "Technicians";
                    case "WORKFORCE_ENGINEERS":
                        return "Engineers";
                    case "WORKFORCE_SCIENTISTS":
                        return "Scientists";
                    case "ADVERTISING_AGRICULTURE":
                        return "Agriculture";
                    case "ADVERTISING_CHEMISTRY":
                        return "Chemistry";
                    case "ADVERTISING_CONSTRUCTION":
                        return "Construction";
                    case "ADVERTISING_ELECTRONICS":
                        return "Electronics";
                    case "ADVERTISING_FOOD_INDUSTRIES":
                        return "Food Industries";
                    case "ADVERTISING_FUEL_REFINING":
                        return "Fuel Refining";
                    case "ADVERTISING_MANUFACTURING":
                        return "Manufacturing";
                    case "ADVERTISING_METALLURGY":
                        return "Metallurgy";
                    case "ADVERTISING_RESOURCE_EXTRACTION":
                        return "Resource Extraction";
                    default:
                        return cogc;
                }
            }

            return "None";
        }
    }
}
