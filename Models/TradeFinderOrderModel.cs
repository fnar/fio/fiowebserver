using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using FIOWebServer.JsonPayloads;
using System.Net.Http;

namespace FIOWebServer.Models
{
    public class TradeFinderOrderModel
    {
        public string CompanyName { get; set; }
        public int? ItemCount { get; set; }
        public double? ItemCost { get; set; }
        public string Currency { get; set; }
        public string CostDisplay
        {
            get
            {
                if (ItemCost == null || Currency == null)
                    return "";
                return $"{ItemCost?.ToString("N2")} {Currency}";
            }
        }

        public static async Task<List<TradeFinderOrderModel>[]> GetTradeFinderOrderModels(string commodityOriginTicker, string commodityDestinationTicker)
        {
            //Orders to BUY from
            var originOrdersTask = new Web.Request(HttpMethod.Get, $"/exchange/{commodityOriginTicker}").GetResponseAsync<ExchangeOrderPayload>();
            //Orders to SELL to
            var destinationOrdersTask = new Web.Request(HttpMethod.Get, $"/exchange/{commodityDestinationTicker}").GetResponseAsync<ExchangeOrderPayload>();

            var originExchange = (await originOrdersTask);
            var destinationExchange = (await destinationOrdersTask);


            var originOrders = (await originOrdersTask).SellingOrders.Select(x => new TradeFinderOrderModel()
            {
                CompanyName = x.CompanyName,
                ItemCount = x.ItemCount,
                ItemCost = x.ItemCost,
                Currency = originExchange.Currency
            }).ToList();

            var destinationOrders = (await destinationOrdersTask).BuyingOrders.Select(x => new TradeFinderOrderModel()
            {
                CompanyName = x.CompanyName,
                ItemCount = x.ItemCount,
                ItemCost = x.ItemCost,
                Currency = destinationExchange.Currency
            }).ToList();

            return new List<TradeFinderOrderModel>[2] {
                originOrders, destinationOrders
            };
        }
    }
}