using System;
using System.Collections.Generic;

namespace FIOWebServer.Models
{
    public class InventoryModel : ICloneable
    {
        public string LocationIdentifier { get; set; } = "";
        public string LocationName { get; set; } = "";

        public string LocationDisplay
        {
            get
            {
                if (LocationName != null && LocationName != LocationIdentifier )
                {
                    return $"{LocationName} ({LocationIdentifier})";
                }

                return LocationIdentifier;
            }
        }

        public string StorageType { get; set; } = "";
        public string StorageIdCommand { get; set; } = "";

        public string MaterialName { get; set; } = "";
        public int? Units { get; set; }

        public List<InventoryModel> Children { get; set; } = new List<InventoryModel>();

        public DateTime LastUpdate { get; set; }

        public object Clone()
        {
            InventoryModel model = new InventoryModel();

            model.LocationIdentifier = LocationIdentifier;
            model.LocationName = LocationName;
            model.StorageType = StorageType;
            model.StorageIdCommand = StorageIdCommand;
            model.MaterialName = MaterialName;
            model.Units = Units;

            foreach( var child in Children )
            {
                model.Children.Add((InventoryModel)child.Clone());
            }

            model.LastUpdate = LastUpdate;

            return model;
        }
    }
}
