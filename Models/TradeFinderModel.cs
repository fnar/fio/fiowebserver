using System.Threading.Tasks;
using System.Collections.Generic;
using System.Net.Http;
using FIOWebServer.JsonPayloads;
using FIOWebServer;
using System.Diagnostics;
using System;
using System.Linq;

namespace FIOWebServer.Models
{
    public class TradeFinderModel
    {
        public string OriginExchangeCode { get; set; }
        public string DestinationExchangeCode { get; set; }

        public string MaterialTicker { get; set; }

        //On Destination
        public double? Bid { get; set; }
        public int? BidVolume { get; set; }
        public string BidCurrency { get; set; }
        public string BidDisplay
        {
            get
            {
                return Bid != null ? $"{Bid?.ToString("F2")} {BidCurrency}" : "";
            }
        }

        //On Origin
        public double? Ask { get; set; }
        public int? AskVolume { get; set; }
        public string AskCurrency { get; set; }
        public string AskDisplay
        {
            get
            {
                return Ask != null ? $"{Ask?.ToString("F2")} {AskCurrency}" : "";
            }
        }

        public int? OperationalVolume
        {
            get
            {
                if (AskVolume == null || BidVolume == null)
                    return null;
                return Math.Min((int)AskVolume, (int)BidVolume);
            }
        }
        public double? Profit
        {
            get
            {
                if (Ask == null || Bid == null || OperationalVolume == null)
                    return null;
                return (Bid * OperationalVolume * (double?)ExchangeCurrencies(BidCurrency, AskCurrency)) - (Ask * OperationalVolume);
            }
        }

        public string ProfitDisplay
        {
            get
            {
                return Profit != null ? $"{Profit?.ToString("F2")} {AskCurrency}" : "";
            }
        }


        public double? ProfitPerUnit
        {
            get
            {
                if (Ask == null || Bid == null || OperationalVolume == null)
                    return null;
                return (Bid * (double?)ExchangeCurrencies(BidCurrency, AskCurrency)) - Ask;
            }
        }

        public string ProfitPerUnitDisplay
        {
            get
            {
                return Profit != null ? $"{ProfitPerUnit?.ToString("F2")} {AskCurrency}" : "";
            }
        }


        public double? CargoWeight { get; set; }
        public double? CargoVolume { get; set; }

        public double? PayoutPricePer500Ton
        {
            get
            {
                if (CargoWeight == null || ProfitPerUnit == null)
                    return null;
                return Math.Floor(500.0 / CargoWeight.Value) * ProfitPerUnit; //Cast down to 0
            }
        }
        public double? PayoutPricePer500MeterSquared
        {
            get
            {
                if (CargoVolume == null || ProfitPerUnit == null)
                    return null;
                return Math.Floor(500.0 / CargoVolume.Value) * ProfitPerUnit; //Cast down to 0
            }
        }

        public double? PayoutPricePer500
        {
            get
            {
                return (CargoWeight >= CargoVolume) ? PayoutPricePer500Ton : PayoutPricePer500MeterSquared;
            }
        }

        public string PayoutPricePer500UnitsDisplay
        {
            get
            {
                if (CargoWeight >= CargoVolume)
                {
                    if (PayoutPricePer500Ton == null)
                        return "";
                    string ppt = PayoutPricePer500Ton?.ToString("N2");
                    return $"{ppt} {AskCurrency}/500t";
                }
                else
                {
                    if (PayoutPricePer500MeterSquared == null)
                        return "";
                    string ppv = PayoutPricePer500MeterSquared?.ToString("N2");
                    return $"{ppv} {AskCurrency}/500m&sup3;";
                }
            }
        }




        public enum TradeFinderSearchType
        {
            Origin, //Origin means that we want to buy our goods here and sell elsewhere
            Destination, //Destination means that we want to sell our goods here and buy elsewhere
            All //All means that it does not matter where we buy or sell, we want the best possible route
        }

        //Hardcoded prices from 24.02.2021 (waiting for FX API endpoint)

        private static decimal? ExchangeCurrencies(string from, string to)
        {
            if (fxDict.TryGetValue((from, to), out decimal v))
                return v;
            return null;
        }


        //Caching dictionaries
        private static Dictionary<string, MaterialPayload> materialDict;
        private static object matDictLock = new object();
        public static Dictionary<string, MaterialPayload> getCopyMaterialDict()
        {
            lock (matDictLock)
            {
                return new Dictionary<string, MaterialPayload>(materialDict);
            }
        }
        private static Dictionary<(string, string), decimal> fxDict = null;




        public static async Task<List<TradeFinderModel>> GetTradeFinderModels(string AuthToken, string targetExchangeCode, TradeFinderSearchType searchType, List<string> filterMaterials)
        {
            //Requests to be fetched everytime
            var tradeFinderRequestTask = new Web.Request(HttpMethod.Get, $"/exchange/all", AuthToken).GetResponseAsync<List<PricePayload>>();
            var comexRequestTask = new Web.Request(HttpMethod.Get, $"/global/comexexchanges").GetResponseAsync<List<ExchangePayload>>();
            if (materialDict == null)
            {
                var materialRequestTask = new Web.Request(HttpMethod.Get, $"/material/allmaterials", AuthToken).GetResponseAsync<List<MaterialPayload>>();
                materialDict = (await materialRequestTask).ToDictionary(x => x.Ticker, x => x);
            }
            if (fxDict == null)
            {
                var fxRequestTask = new Web.Request(HttpMethod.Get, $"/currency/all", AuthToken).GetResponseAsync<List<CurrencyPayload>>();
                fxDict = (await fxRequestTask).ToDictionary(x => (x.BaseCurrencyCode.ToString(), x.QuoteCurrencyCode.ToString()), x => x.Open);
            }

            var prices = await tradeFinderRequestTask;
            var priceDict = new Dictionary<string, List<PricePayload>>();
            if ( filterMaterials.Count > 0 )
            {
                priceDict = prices.Where(x=> filterMaterials.Contains(x.MaterialTicker.ToUpper())).GroupBy(x => x.MaterialTicker).ToDictionary(x => x.Key, x => x.ToList()); //bin by commodity
            }
            else
            {
                priceDict = prices.GroupBy(x => x.MaterialTicker).ToDictionary(x => x.Key, x => x.ToList()); //bin by commodity

            }

            var exCodeToCurrencyCodeDict = (await comexRequestTask).GroupBy(x => x.ExchangeCode).Select(x => x.First())
            .ToDictionary(x => x.ExchangeCode, x => x.CurrencyCode);

            var result = new List<TradeFinderModel>();

            foreach (var kv in priceDict)
            {
                var commodityPrices = kv.Value;

                foreach (var p in commodityPrices) //Create all exchange permutations
                {
                    foreach (var p2 in commodityPrices)
                    {
                        if (p == p2)
                            continue;

                        TradeFinderModel r = new TradeFinderModel();
                        r.MaterialTicker = kv.Key;
                        if (materialDict?.ContainsKey(r.MaterialTicker) == true)
                        {
                            r.CargoVolume = materialDict[r.MaterialTicker].Volume;
                            r.CargoWeight = materialDict[r.MaterialTicker].Weight;
                        }

                        r.OriginExchangeCode = p.ExchangeCode;
                        r.Ask = p.Ask;
                        r.AskVolume = p.AskCount;
                        r.AskCurrency = exCodeToCurrencyCodeDict[r.OriginExchangeCode];

                        r.DestinationExchangeCode = p2.ExchangeCode;
                        r.Bid = p2.Bid;
                        r.BidVolume = p2.BidCount;
                        r.BidCurrency = exCodeToCurrencyCodeDict[r.DestinationExchangeCode];

                        //Do not add row if there is no bid and ask
                        if (r.Ask != null || r.Bid != null)
                            result.Add(r);
                    }
                }
            }
            if (!string.IsNullOrEmpty(targetExchangeCode))
                switch (searchType) //Filter
                {
                    case TradeFinderSearchType.Origin:
                        result = result.Where(x => x.OriginExchangeCode == targetExchangeCode).ToList();
                        break;
                    case TradeFinderSearchType.Destination:
                        result = result.Where(x => x.DestinationExchangeCode == targetExchangeCode).ToList();
                        break;
                    case TradeFinderSearchType.All:
                        result = result.Where(x => x.DestinationExchangeCode == targetExchangeCode || x.OriginExchangeCode == targetExchangeCode).ToList();
                        break;
                }

            return result;
        }
    }


}