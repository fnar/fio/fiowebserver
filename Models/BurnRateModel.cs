using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

using FIOWebServer.JsonPayloads;

namespace FIOWebServer.Models
{
    public class BurnRateModel
    {
        public BurnRateModel Parent = null;

        public bool IsInExclusionList { get; set; }

        public string PlanetId { get; set; }
        public string PlanetName { get; set; }
        public string PlanetNaturalId { get; set; }
        public string PlanetDisplayName
        {
            get
            {
                return (PlanetName != PlanetNaturalId) ? $"{PlanetNaturalId} ({PlanetName})" : PlanetNaturalId;
            }
            set
            {
                // Intentionally empty
            }
        }

        public string PlanetSiteId { get; set; }
        public string PlanetStorageId { get; set; }

        public string Material { get; set; }
        public double MaterialWeight { get; set; }
        public double MaterialVolume { get; set; }

        public DateTime? ConsumptionTime { get; set; }

        public DateTime LastUpdate { get; set; } = DateTime.MaxValue;
        public string LastUpdateCause { get; set; } = "";

        public float? BurnRate { get; set; }
        public string BurnRateDisplay
        {
            get
            {
                return BurnRate != null ? String.Format("{0:0.####}", (float)BurnRate) : "";
            }
        }

        public int? UnitsOnPlanet { get; set; }
        public string UnitsOnPlanetDisplay
        {
            get
            {
                return UnitsOnPlanet != null ? ((int)UnitsOnPlanet).ToString() : "";
            }
        }

        public float? DaysRemaining
        {
            get
            {
                if (Children != null && Children.Count > 0 )
                {
                    float? ParentDaysRemaining = null;
                    foreach(var child in Children)
                    {
                        if (!child.IsInExclusionList && child.BurnRate != null)
                        {
                            float childBurnRate = (float)child.BurnRate;
                            if ( child.UnitsOnPlanet != null )
                            {
                                float childUnitsOnPlanet = (float)child.UnitsOnPlanet;
                                float childDaysRemaining = (childUnitsOnPlanet > 0) ? childUnitsOnPlanet / childBurnRate : 0.0f;
                                
                                if ( ParentDaysRemaining == null)
                                {
                                    ParentDaysRemaining = childDaysRemaining;
                                }
                                else
                                {
                                    ParentDaysRemaining = Math.Min((float)ParentDaysRemaining, childDaysRemaining);
                                }
                            }
                        }
                    }

                    return ParentDaysRemaining;
                }
                else
                {
                    if ( BurnRate != null )
                    {
                        if (UnitsOnPlanet != null && UnitsOnPlanet > 0)
                        {
                            return UnitsOnPlanet / BurnRate;
                        }
                        else
                        {
                            return 0.0f;
                        }
                        
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            set
            {
                // Intentionally left empty
            }
        }
        public string DaysRemainingDisplay
        {
            get
            {
                return (DaysRemaining != null) ? String.Format("{0:0.##}", (float)DaysRemaining) : "";
            }
        }

        public int? UnitsToGetToYellow { get; set; }
        public string UnitsToGetToYellowDisplay
        {
            get
            {
                if (FilteredChildren.Count > 0) // Top-levels ignore excluded
                {
                    if (WeightToGetToYellow != null && VolumeToGetToYellow != null)
                    {
                        return $"({WeightToGetToYellow:N2}t / {VolumeToGetToYellow:N2}m&sup3;)";
                    }
                }
                else
                {
                    if (UnitsToGetToYellow != null)
                    {
                        return $"{UnitsToGetToYellow}";
                    }
                }

                return "";
            }
        }

        public double? WeightToGetToYellow
        {
            get
            {
                if (FilteredChildren.Count > 0) // Top-levels ignore excluded
                {
                    double ParentWeight = 0.0;
                    foreach (var child in FilteredChildren)
                    {
                        if (child.WeightToGetToYellow != null)
                        {
                            ParentWeight += (double)child.WeightToGetToYellow;
                        }
                    }

                    if (ParentWeight != 0.0)
                    {
                        return ParentWeight;
                    }
                }
                else
                {
                    if (UnitsToGetToYellow != null)
                    {
                        return UnitsToGetToYellow * MaterialWeight;
                    }
                }

                return null;
            }
        }

        public double? VolumeToGetToYellow
        {
            get
            {
                if (FilteredChildren.Count > 0)
                {
                    double ParentVolume = 0.0;
                    foreach (var child in FilteredChildren)
                    {
                        if (child.VolumeToGetToYellow != null)
                        {
                            ParentVolume += (double)child.VolumeToGetToYellow;
                        }
                    }

                    if (ParentVolume != 0.0)
                    {
                        return ParentVolume;
                    }
                }
                else
                {
                    if (UnitsToGetToYellow != null)
                    {
                        return UnitsToGetToYellow * MaterialVolume;
                    }
                }

                return null;
            }
        }

        public List<BurnRateModel> FilteredChildren
        {
            get
            {
                return Children.Where(c => !c.IsInExclusionList).ToList();
            }
        }
        public List<BurnRateModel> Children { get; set; } = new List<BurnRateModel>();

        public static async Task<List<BurnRateModel>> GetBurnRateModels(string UserName, string AuthToken, List<MaterialPayload> AllConsumables, List<PlanetBurnRateSettings> allBurnRateSettings)
        {
            List<BurnRateModel> brModels = null;

            // Workforce data
            var workforceRequest = new Web.Request(HttpMethod.Get, $"/workforce/{UserName}", AuthToken);
            var allWorkforces = await workforceRequest.GetResponseAsync<List<WorkforcePayload>>();

            // Storage data
            var storageRequest = new Web.Request(HttpMethod.Get, $"/storage/{UserName}", AuthToken);
            var allStorage = await storageRequest.GetResponseAsync<List<StoragePayload>>();

            if (allWorkforces != null && allStorage != null)
            {
                brModels = new List<BurnRateModel>();
                foreach (var planetWorkforces in allWorkforces)
                {
                    DateTime workforceTimestamp = planetWorkforces.Timestamp;

                    BurnRateModel planetWM = new BurnRateModel();
                    foreach (var planetWorkforce in planetWorkforces.Workforces)
                    {   
                        if (planetWorkforce.Population > 0 && planetWorkforce.WorkforceNeeds.Sum(wn => wn.UnitsPerInterval) > 0)
                        {
                            planetWM.PlanetId = planetWorkforces.PlanetId;
                            planetWM.PlanetName = planetWorkforces.PlanetName;
                            planetWM.PlanetNaturalId = planetWorkforces.PlanetNaturalId;
                            planetWM.PlanetDisplayName = (planetWM.PlanetNaturalId != planetWM.PlanetName) ? $"{planetWM.PlanetName} ({planetWM.PlanetNaturalId})" : planetWM.PlanetNaturalId;
                            planetWM.PlanetSiteId = planetWorkforces.SiteId;
                            planetWM.Material = String.Empty;

                            if ( planetWorkforces.LastWorkforceUpdateTime != null)
                            {
                                TimeSpan LastWorkforceUpdateTimeThreshold = new TimeSpan(7, 0, 0, 0);
                                if (DateTime.UtcNow - planetWorkforces.LastWorkforceUpdateTime < LastWorkforceUpdateTimeThreshold)
                                {
                                    planetWM.ConsumptionTime = planetWorkforces.LastWorkforceUpdateTime;
                                }
                            }
                            
                            planetWM.LastUpdate = workforceTimestamp;
                            planetWM.LastUpdateCause = "WORKFORCE";
                            planetWM.BurnRate = null;
                            planetWM.UnitsOnPlanet = null;

                            var planetBurnRateSettings = allBurnRateSettings.Where(brs => brs.PlanetNaturalId.ToUpper() == planetWM.PlanetNaturalId.ToUpper()).FirstOrDefault();

                            foreach (var workforceNeed in planetWorkforce.WorkforceNeeds)
                            {
                                string material = workforceNeed.MaterialTicker;

                                bool bMaterialShouldBeHidden = false;
                                if (planetBurnRateSettings != null)
                                {
                                    bMaterialShouldBeHidden = planetBurnRateSettings.MaterialExclusions.Any(me => me.MaterialTicker.ToUpper() == material.ToUpper());
                                }

                                var wm = planetWM.Children.Where(c => c.Material == material).FirstOrDefault();
                                bool bFoundExisting = (wm != null);
                                if (!bFoundExisting)
                                {
                                    wm = new BurnRateModel();
                                    wm.Parent = planetWM;

                                    wm.IsInExclusionList = bMaterialShouldBeHidden;
                                    wm.Material = material;
                                    var materialObj = AllConsumables.Where(c => c.Ticker == material).FirstOrDefault();
                                    wm.MaterialWeight = materialObj.Weight;
                                    wm.MaterialVolume = materialObj.Volume;

                                    var planetStorage = allStorage.Where(s => s.AddressableId == planetWorkforces.SiteId && s.StorageId != null).FirstOrDefault();
                                    if ( planetStorage != null )
                                    {
                                        planetWM.PlanetStorageId = planetStorage.StorageId;

                                        if (planetStorage.Timestamp < planetWM.LastUpdate)
                                        {
                                            planetWM.LastUpdate = planetStorage.Timestamp;
                                            planetWM.LastUpdateCause = "STORAGE";
                                        }

                                        var storageItem = (planetStorage != null) ? planetStorage.StorageItems.Where(si => si.MaterialTicker == wm.Material).FirstOrDefault() : null;
                                        wm.UnitsOnPlanet = (storageItem != null) ? storageItem.MaterialAmount : 0;

                                        if (workforceNeed.UnitsPerInterval > 0.0f)
                                        {
                                            wm.BurnRate = workforceNeed.UnitsPerInterval;
                                        }

                                        planetWM.Children.Add(wm);
                                    }
                                }
                                else
                                {
                                    if (workforceNeed.UnitsPerInterval > 0.0f)
                                    {
                                        if (wm.BurnRate == null)
                                        {
                                            wm.BurnRate = workforceNeed.UnitsPerInterval;
                                        }
                                        else
                                        {
                                            wm.BurnRate += workforceNeed.UnitsPerInterval;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if (planetWM.PlanetId != null)
                    {
                        planetWM.Children = planetWM.Children.OrderBy(c => c.Material).ToList();
                        brModels.Add(planetWM);
                    }
                }
            }

            return brModels;
        }

        public void UpdateYellowThreshold(int YellowThreshold)
        {
            foreach( var child in Children )
            {
                int childDaysRemaining = (int)Math.Floor((child.DaysRemaining != null) ? (float)child.DaysRemaining : 0.0f);
                float childBurnRate = (child.BurnRate != null) ? (float)child.BurnRate : 0.0f;
                int daysOfResourcesNeeded = YellowThreshold - childDaysRemaining;
                child.UnitsToGetToYellow = (daysOfResourcesNeeded > 0) ? (int)Math.Ceiling(daysOfResourcesNeeded * childBurnRate) : null;
            }
        }
    }
}
