using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

using FIOWebServer.Models;
using FIOWebServer.JsonPayloads;

using MatBlazor;
using AntDesign;
using Microsoft.AspNetCore.Components;

namespace FIOWebServer.Pages
{
    public partial class CXTransactionViewer : ComponentBase
    {
        private MatTheme blackTheme = new MatTheme()
        {
            Primary = "black",
            Secondary = "black"
        };

        private int CurrentProgress = 0;

        private List<string> MaterialOptions;
        private List<string> ExchangeOptions;

        private string SelectedMaterial;
        private string SelectedExchange;
        private double HoursToShow = 4.0;

        private List<CXPCModel> transactions = null;

        protected override async Task OnInitializedAsync()
        {
            GlobalAppState.OnChange += StateHasChanged;

            await Task.Delay(3000);

            SetProgress(0);
            var request = new Web.Request(HttpMethod.Get, "/material/allmaterials");
            var  AllMaterials = await request.GetResponseAsync<List<MaterialPayload>>();
            MaterialOptions = AllMaterials.Select(m => m.Ticker).ToList();
            MaterialOptions.Sort();

            SetProgress(1);
            request = new Web.Request(HttpMethod.Get, "/global/comexexchanges");
            var AllExchanges = (await request.GetResponseAsync<List<ExchangePayload>>()).OrderBy(ep => ep.ExchangeCode).ToList();
            ExchangeOptions = AllExchanges.Select(e => e.ExchangeCode).ToList();
            ExchangeOptions.Sort();

            SetProgress(2);

            if (NavManager.TryGetQueryString<string>("Material", out string QuerySelectedMaterial))
			{
                SelectedMaterial = QuerySelectedMaterial.ToUpper();
                if (!MaterialOptions.Contains(SelectedMaterial))
				{
                    SelectedMaterial = null;
                }
            }
            
            if (NavManager.TryGetQueryString<string>("Exchange", out string QuerySelectedExchange))
			{
                SelectedExchange = QuerySelectedExchange.ToUpper();
                if (!ExchangeOptions.Contains(SelectedExchange))
				{
                    SelectedExchange = null;
				}
            }
            
            if (NavManager.TryGetQueryString<decimal>("HoursToShow", out decimal QueryHoursToShow))
			{
                HoursToShow = Math.Clamp(decimal.ToDouble(QueryHoursToShow), 1.0, 100.0);
			}

            if (SelectedMaterial != null && SelectedExchange != null)
			{
                await RetrieveClick();
			}
        }

        protected void SetProgress(int value)
        {
            CurrentProgress = value;
            StateHasChanged();
        }

        string tipFormatter(double currentValue)
		{
            return $"{currentValue:N2}";
		}

        private bool Retrieving = false;
        private async Task RetrieveClick()
		{
            if (String.IsNullOrEmpty(SelectedMaterial) || String.IsNullOrEmpty(SelectedExchange))
            {
                return;
            }

            Retrieving = true;
            StateHasChanged();

            long Timestamp = DateTime.UtcNow.AddHours(-HoursToShow).ToUnixTime();
            var request = new Web.Request(HttpMethod.Get, $"/exchange/cxpc/{SelectedMaterial}.{SelectedExchange}/{Timestamp}");
            transactions = await request.GetResponseAsync<List<CXPCModel>>();

            NavManager.NavigateTo($"/cxtransactionviewer?Material={SelectedMaterial}&Exchange={SelectedExchange}&HoursToShow={HoursToShow:N2}");

            Retrieving = false;
            StateHasChanged();
        }
    }
}