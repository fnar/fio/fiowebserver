using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Timers;
using System.Threading.Tasks;

using FIOWebServer.JsonPayloads;

using Newtonsoft.Json;

using MatBlazor;
using Microsoft.AspNetCore.Components;
using Timer = System.Timers.Timer;

namespace FIOWebServer.Pages
{
    public partial class GroupHub : ComponentBase, IDisposable
    {
        private Timer _timer = null;

        private List<string> UsersToRetrieve = null;

        private int ProgressCount = 0;
        private GroupHubModel GroupHubModel = null;

        protected override async Task OnInitializedAsync()
        {
            GlobalAppState.OnChange += StateHasChanged;

            if (NavManager.TryGetQueryString("Users", out string Users))
            {
                var UsersSplitRes = Users.Split(new String[] { "__" }, StringSplitOptions.RemoveEmptyEntries);
                UsersToRetrieve = UsersSplitRes.ToList();
            }
            else if (NavManager.TryGetQueryString("Group", out string Group))
			{
                var GroupSplitRes = Group.Split(new char[] { '-' }, StringSplitOptions.RemoveEmptyEntries);
                if (GroupSplitRes.Length > 0)
				{
                    int GroupId;
                    if (int.TryParse(GroupSplitRes[0], out GroupId))
					{
                        var groupRequest = new Web.Request(HttpMethod.Get, $"/auth/group/{GroupId}", await GlobalAppState.GetAuthToken());
                        var group = await groupRequest.GetResponseAsync<GroupPayload>();
                        UsersToRetrieve = group.GroupUsers.Select(gu => gu.GroupUserName).ToList();
					}
				}
			}

            _timer = new Timer(60 * 1000);
            _timer.Elapsed += NotifyElapsed;
            _timer.AutoReset = true;
            _timer.Enabled = true;

            await Task.FromResult(0);
        }

        private void NotifyElapsed(object source, ElapsedEventArgs e)
        {
            StateHasChanged();
        }

        protected override async Task OnAfterRenderAsync(bool firstRender)
        {
            if (!firstRender && GlobalAppState.State == LoadState.Authenticated && GroupHubModel == null)
            {
                await Refresh();
            }
        }

        private bool InRefresh = false;
        private async Task Refresh()
        {
            if (!InRefresh)
            {
                InRefresh = true;
                GroupHubModel = null;

                ProgressCount = 0;
                StateHasChanged();

                var groupHubRequest = new Web.Request(HttpMethod.Post, "/fioweb/grouphub", await GlobalAppState.GetAuthToken(), JsonConvert.SerializeObject(UsersToRetrieve));
                GroupHubModel = await groupHubRequest.GetResponseAsync<GroupHubModel>();
                CleanupGroupHubModel();

                ProgressCount = 1;

                InRefresh = false;
                StateHasChanged();
            }
        }

        private DateTime TimeThreshold = DateTime.UtcNow.AddDays(-7.0);
        private void CleanupGroupHubModel()
		{
            // Reap old CXWarehouse data
            for (int CXWarehouseIdx = GroupHubModel.CXWarehouses.Count - 1; CXWarehouseIdx >= 0; --CXWarehouseIdx)
			{
                var CXWarehouse = GroupHubModel.CXWarehouses[CXWarehouseIdx];
                for (int PlayerCXWarehouseIdx = CXWarehouse.PlayerCXWarehouses.Count -1; PlayerCXWarehouseIdx >= 0; --PlayerCXWarehouseIdx)
				{
                    var PlayerCXWarehouse = CXWarehouse.PlayerCXWarehouses[PlayerCXWarehouseIdx];
                    if ( PlayerCXWarehouse.LastUpdated < TimeThreshold)
					{
                        CXWarehouse.PlayerCXWarehouses.RemoveAt(PlayerCXWarehouseIdx);
					}
                }

                if ( CXWarehouse.PlayerCXWarehouses.Count == 0)
				{
                    GroupHubModel.CXWarehouses.RemoveAt(CXWarehouseIdx);
				}
			}

            // Reap old PlayerShipsInFlightData
            GroupHubModel.PlayerShipsInFlight.RemoveAll(ps => ps.LastUpdated < TimeThreshold);

            for( int PlayerModelIdx = GroupHubModel.PlayerModels.Count - 1; PlayerModelIdx >= 0; --PlayerModelIdx)
			{
                var PlayerModel = GroupHubModel.PlayerModels[PlayerModelIdx];
                PlayerModel.Currencies.RemoveAll(c => c.LastUpdated < TimeThreshold);
                PlayerModel.Locations.RemoveAll(l =>
                    ((l.BaseStorage == null || l.BaseStorage.LastUpdated < TimeThreshold) &&
                    (l.WarehouseStorage == null || l.WarehouseStorage.LastUpdated < TimeThreshold))
                );
                
                foreach (var Location in PlayerModel.Locations)
				{
                    foreach (var ProdLine in Location.ProductionLines)
					{
                        ProdLine.ProductionOrders = 
                            ProdLine.ProductionOrders
                            .OrderByDescending(po => po.TimeCompletion.HasValue) // nulls go at the end
                            .ThenByDescending(po => po.Started)
                            .ThenBy(po => po.TimeCompletion)
                            .ToList();
					}
				}
			}

		}

        private double getDailyConsumption(TimeSpan duration, int amount)
        {
            return Math.Round(new TimeSpan(24, 0, 0).Divide(duration) * amount, 1);
        }

        public void Dispose()
        {
            GlobalAppState.OnChange -= StateHasChanged;
        }
    }
}