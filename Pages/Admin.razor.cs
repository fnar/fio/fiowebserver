using System.Text;
using System.Net;

using MatBlazor;
using Microsoft.AspNetCore.Components.Web;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Components;

namespace FIOWebServer.Pages
{
    public partial class Admin : ComponentBase, IDisposable
    {
        protected override void OnInitialized()
        {
            GlobalAppState.OnChange += StateHasChanged;
        }

        public void Dispose()
        {
            GlobalAppState.OnChange -= StateHasChanged;
        }

        // Not cryptographically secure, but who cares.
        private static Random rnd = new Random();
        private static string GenerateRandomPassword(int length)
        {
            const string validCharacters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!@#$%^&*";
            StringBuilder res = new StringBuilder();
            while (0 < length--)
            {
                res.Append(validCharacters[rnd.Next(validCharacters.Length)]);
            }

            return res.ToString();
        }

        private async Task OnCreateAccountClicked()
        {
            createAccountPassword = GenerateRandomPassword(16);
            createAccountDlgVisible = true;
            await Task.Delay(500);
        }

        private string createAccountUserName = "";
        private string createAccountPassword = "";

        private bool createAccountIsAdmin = false;
        void ToggleAdminChecked(bool value)
        {
            createAccountIsAdmin = value;
        }

        JsonCreateUserPayload createUserPayload = null;

        private bool createAccountDlgVisible = false;
        private bool userAlreadyExistsDlgVisible = false;
        private async Task OnCreateAccountOk(MouseEventArgs e)
        {
            if (createAccountUserName.Length < 3)
            {
                createAccountDlgVisible = false;
                await Task.Delay(500);
                return;
            }

            createUserPayload = new JsonCreateUserPayload();
            createUserPayload.UserName = createAccountUserName;
            createUserPayload.Password = createAccountPassword;
            createUserPayload.IsAdmin = createAccountIsAdmin;

            // Check to see if the user exists
            var userExistsRequest = new Web.Request(HttpMethod.Get, $"/admin/{createUserPayload.UserName}", await GlobalAppState.GetAuthToken());
            await userExistsRequest.GetResultNoResponse();
            if (userExistsRequest.StatusCode == HttpStatusCode.OK)
            {
                createAccountDlgVisible = false;
                await Task.Delay(500);

                // User exists
                userAlreadyExistsDlgVisible = true;
                return;
            }

            await CreateUser();
            createAccountDlgVisible = false;
            await Task.Delay(500);
        }

        private async Task OnCreateAccountCancel(MouseEventArgs e)
        {
            createAccountDlgVisible = false;
            await Task.Delay(500);
        }

        private async Task OnUserAlreadyExistsOk(MouseEventArgs e)
        {
            await CreateUser();
            userAlreadyExistsDlgVisible = false;
            await Task.Delay(500);
        }

        private async Task OnUserAlreadyExistsCancel(MouseEventArgs e)
        {
            userAlreadyExistsDlgVisible = false;
            await Task.Delay(500);
        }

        private string RecentAccountInfo = null;
        private async Task CreateUser()
        {
            var createUserRequest = new Web.Request(HttpMethod.Post, "/admin/create", await GlobalAppState.GetAuthToken(), JsonConvert.SerializeObject(createUserPayload));
            await createUserRequest.GetResultNoResponse();
            if (createUserRequest.StatusCode == HttpStatusCode.OK)
            {
                string discordAccountClipboardContents = null;
                try
                {
                    discordAccountClipboardContents = $"Account information:\r\n```\r\nUserName: {createUserPayload.UserName}\r\nPassword: {createUserPayload.Password}\r\n```";
                    await Clipboard.WriteTextAsync(discordAccountClipboardContents);
                    Toaster.Add("Account created.  Details in clipboard.", MatToastType.Info, "Created");
                }
                catch
                {
                    Toaster.Add("Browser failed to allow clipboard copying.", MatToastType.Danger, "Clipboard Failure");
                    RecentAccountInfo = discordAccountClipboardContents;
                }
            }
            else
            {
                Toaster.Add("Failed to create account.", MatToastType.Danger, "Failed");
            }
        }

        private async Task CopyAccountInfo()
        {
            try
            {
                await Clipboard.WriteTextAsync(RecentAccountInfo);
                Toaster.Add("Account details in clipboard.", MatToastType.Info, "Copied");
            }
            catch
            {

            }
        }

        private bool areYouSureDlgVisible = false;
        private string clearCommand = null;

        private async Task AreYouSureOnOk()
        {
            switch (clearCommand)
            {
                case "cx":
                    await OnClearCXData();
                    break;
                case "bui":
                    await OnClearBUIData();
                    break;
                case "mat":
                    await OnClearMATData();
                    break;
                case "chat":
                    await OnClearChatData();
                    break;
                case "jump":
                    await OnClearJumpCache();
                    break;
            }

            areYouSureDlgVisible = false;
            await Task.Delay(500);
        }

        private async Task AreYouSureOnCancel()
        {
            areYouSureDlgVisible = false;
            await Task.Delay(500);
        }

        private async Task OnClearCXData()
        {
            var clearCxDataRequest = new Web.Request(HttpMethod.Post, "/admin/clearcxdata", await GlobalAppState.GetAuthToken());
            await clearCxDataRequest.GetResultNoResponse();
            Toaster.Add("CX Data Cleared.", MatToastType.Info, "Cleared");
        }

        private async Task OnClearBUIData()
        {
            var clearBuiDataRequest = new Web.Request(HttpMethod.Post, "/admin/clearbuildingdata", await GlobalAppState.GetAuthToken());
            await clearBuiDataRequest.GetResultNoResponse();
            Toaster.Add("BUI Data Cleared.", MatToastType.Info, "Cleared");
        }

        private async Task OnClearMATData()
        {
            var clearMatDataRequest = new Web.Request(HttpMethod.Post, "/admin/clearmatdata", await GlobalAppState.GetAuthToken());
            await clearMatDataRequest.GetResultNoResponse();
            Toaster.Add("MAT Data Cleared.", MatToastType.Info, "Cleared");
        }

        private async Task OnClearChatData()
        {
            var clearChatDataRequest = new Web.Request(HttpMethod.Post, "/chat/clear", await GlobalAppState.GetAuthToken());
            await clearChatDataRequest.GetResultNoResponse();
            Toaster.Add("Chat Data Cleared.", MatToastType.Info, "Cleared");
        }

        private async Task OnClearJumpCache()
        {
            var clearJumpCacheRequest = new Web.Request(HttpMethod.Post, "/admin/clearjumpcache", await GlobalAppState.GetAuthToken());
            await clearJumpCacheRequest.GetResultNoResponse();
            Toaster.Add("Jump Cache Cleared.", MatToastType.Info, "Cleared");
        }
    }
}