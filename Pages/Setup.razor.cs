using System.Net;

using MatBlazor;
using Microsoft.AspNetCore.Components;
using Timer = System.Timers.Timer;

namespace FIOWebServer.Pages
{
    public partial class Setup : ComponentBase, IDisposable
    {
        private int StepCurrent = 0;
        private string LoginStatus = "process";
        private string RefreshApexStatus = "wait";
        private string OpenAllBasesStatus = "wait";
        private string DoneStatus = "wait";

        private int SitesCount = 0;
        private int WorkforcesCount = 0;

        private Timer timer = null;

        protected override void OnInitialized()
        {
            GlobalAppState.OnChange += StateHasChanged;

            timer = new Timer(2000.0);
            timer.Elapsed += async delegate { await NotifyTimerElapsed(); };
            timer.AutoReset = false;
            timer.Enabled = true;
        }

        public void Dispose()
        {
            timer.Stop();
            GlobalAppState.OnChange -= StateHasChanged;
        }

        private async Task ProgressToStep(int stepNum)
        {
            StepCurrent = stepNum;
            switch (stepNum)
            {
                case 1:
                    LoginStatus = "finish";
                    RefreshApexStatus = "process";
                    break;
                case 2:
                    RefreshApexStatus = "finish";
                    OpenAllBasesStatus = "process";
                    break;
                case 3:
                    OpenAllBasesStatus = "finish";
                    DoneStatus = "process";
                    StateHasChanged();
                    Toaster.Add("Successfully retrieved all data. Redirecting...", MatToastType.Success, "Success");
                    await Task.Delay(5000);
                    NavManager.NavigateTo("/");
                    break;
            }

            StateHasChanged();
        }

        private async Task NotifyTimerElapsed()
        {
            timer.Stop();
            if (LoginStatus == "process")
            {
                if (GlobalAppState.State == LoadState.Authenticated)
                {
                    await ProgressToStep(1);
                }
            }
            else if (RefreshApexStatus == "process")
            {
                string UserName = await GlobalAppState.GetUserName();
                string AuthToken = await GlobalAppState.GetAuthToken();

                var sitesRequest = new Web.Request(HttpMethod.Get, $"/sites/{UserName}", AuthToken);
                var sitesResponse = await sitesRequest.GetResponseAsync<List<JsonPayloads.SitesPayload>>();
                if (sitesRequest.StatusCode == HttpStatusCode.OK && sitesResponse != null && sitesResponse.Count > 0)
                {
                    SitesCount = sitesResponse.Count;
                    await ProgressToStep(2);
                }
            }
            else if (OpenAllBasesStatus == "process")
            {
                string UserName = await GlobalAppState.GetUserName();
                string AuthToken = await GlobalAppState.GetAuthToken();

                var workforceRequest = new Web.Request(HttpMethod.Get, $"/workforce/{UserName}", AuthToken);
                var workforceResponse = await workforceRequest.GetResponseAsync<List<JsonPayloads.WorkforcePayload>>();
                if (workforceRequest.StatusCode == HttpStatusCode.OK && workforceResponse != null)
                {
                    WorkforcesCount = workforceResponse.Count;
                    StateHasChanged();
                    if (SitesCount <= WorkforcesCount)
                    {
                        await ProgressToStep(3);
                    }
                }
            }

            timer.Start();
        }
    }
}