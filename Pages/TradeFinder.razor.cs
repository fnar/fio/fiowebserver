using Microsoft.AspNetCore.Components;

using MatBlazor;

using FIOWebServer.Models;
using FIOWebServer.JsonPayloads;

namespace FIOWebServer.Pages
{
    public partial class TradeFinder : ComponentBase, IDisposable
    {
        private int TradeSearchPageSize = 50;
        private MatTheme blackTheme = new MatTheme()
        { Primary = "black", Secondary = "black" };
        private volatile bool SearchLoading = false;
        private volatile List<string> availableExchanges = new List<string>();
        private string _SelectedSearchOption = "Origin";
        private string SelectedSearchOption
        {
            get
            {
                return _SelectedSearchOption;
            }

            set
            {
                if (_SelectedSearchOption != value)
                {
                    _SelectedSearchOption = value;
                    StateHasChanged();
                    _ = GlobalAppState.SSSet_Generic<string>("TradeFinderSelectedSearchOption", _SelectedSearchOption);
                }
            }
        }

        private List<string> SearchOptions = new List<string> { "Origin", "Destination", "All" };
        private string _ExchangeSearchString = null;
        private string ExchangeSearchString
        {
            get
            {
                return _ExchangeSearchString;
            }

            set
            {
                if (_ExchangeSearchString != value)
                {
                    _ExchangeSearchString = value;
                    StateHasChanged();
                    _ = GlobalAppState.SSSet_Generic<string>("TradeFinderExchangeSearchString", _ExchangeSearchString);
                }
            }
        }

        private List<string> FilterMaterials = new List<string>();
        private List<TradeFinderModel> TradeSearchModels = new List<TradeFinderModel>();
        private List<TradeFinderOrderModel> OriginOrderModelsLists = new List<TradeFinderOrderModel>();
        private List<TradeFinderOrderModel> DestinationOrderModelsList = new List<TradeFinderOrderModel>();
        private Dictionary<string, MaterialPayload> materials = new Dictionary<string, MaterialPayload>();
        private string OriginExchangeTicker = "";
        private string DestinationExchangeTicker = "";

        protected override async Task OnInitializedAsync()
        {
            await Task.Delay(3000);

            //Same API call is being made at the model, consider using cache
            availableExchanges = (await new Web.Request(HttpMethod.Get, $"/global/comexexchanges").GetResponseAsync<List<ExchangePayload>>()).Select(x => x.ExchangeCode).ToList();
            SelectedSearchOption = await GlobalAppState.SSGet_Generic<string>("TradeFinderSelectedSearchOption");
            ExchangeSearchString = await GlobalAppState.SSGet_Generic<string>("TradeFinderExchangeSearchString");

            var uriQuery = NavManager.ToAbsoluteUri(NavManager.Uri).Query;
            if (uriQuery.ToUpper().StartsWith("?MATS="))
            {
                uriQuery = uriQuery.Substring(6).ToUpper();
                FilterMaterials = uriQuery.Split(new char[] { '&' }, StringSplitOptions.RemoveEmptyEntries).ToList();
            }

            GlobalAppState.OnChange += StateHasChanged;
            StateHasChanged();            
        }

        public void Dispose()
        {
            GlobalAppState.OnChange -= StateHasChanged;
        }

        private async Task SearchClick()
        {
            if (SearchLoading != false) //Do not allow simultaneous searches
                return;
            SearchLoading = true;
            TradeFinderModel.TradeFinderSearchType searchType = TradeFinderModel.TradeFinderSearchType.All;
            if (SelectedSearchOption == "Origin")
            {
                searchType = TradeFinderModel.TradeFinderSearchType.Origin;
            }
            else if (SelectedSearchOption == "Destination")
            {
                searchType = TradeFinderModel.TradeFinderSearchType.Destination;
            }

            TradeSearchModels = await TradeFinderModel.GetTradeFinderModels(await GlobalAppState.GetAuthToken(), ExchangeSearchString, searchType, FilterMaterials);
            materials = TradeFinderModel.getCopyMaterialDict();
            SearchLoading = false;
        }

        private void SortShipmentData(MatSortChangedEvent sort)
        {
            if (sort != null && sort.Direction != MatSortDirection.None && !String.IsNullOrWhiteSpace(sort.SortId))
            {
                bool bAscending = (sort.Direction == MatSortDirection.Asc);
                switch (sort.SortId)
                {
                    case "OriginExchangeCode":
                        TradeSearchModels = bAscending ? TradeSearchModels.OrderBy(tsm => tsm.OriginExchangeCode).ToList() : TradeSearchModels.OrderByDescending(tsm => tsm.OriginExchangeCode).ToList();
                        break;
                    case "DestinationExchangeCode":
                        TradeSearchModels = bAscending ? TradeSearchModels.OrderBy(tsm => tsm.DestinationExchangeCode).ToList() : TradeSearchModels.OrderByDescending(tsm => tsm.DestinationExchangeCode).ToList();
                        break;
                    case "CommodityTicker":
                        TradeSearchModels = bAscending ? TradeSearchModels.OrderBy(tsm => tsm.MaterialTicker).ToList() : TradeSearchModels.OrderByDescending(tsm => tsm.MaterialTicker).ToList();
                        break;
                    case "Bid":
                        TradeSearchModels = bAscending ? TradeSearchModels.OrderBy(tsm => tsm.Bid != null ? tsm.Bid : double.MaxValue).ToList() : TradeSearchModels.OrderByDescending(tsm => tsm.Bid).ToList();
                        break;
                    case "BidVolume":
                        TradeSearchModels = bAscending ? TradeSearchModels.OrderBy(tsm => tsm.BidVolume != null ? tsm.BidVolume : int.MaxValue).ToList() : TradeSearchModels.OrderByDescending(tsm => tsm.BidVolume).ToList();
                        break;
                    case "Ask":
                        TradeSearchModels = bAscending ? TradeSearchModels.OrderBy(tsm => tsm.Ask != null ? tsm.Ask : double.MaxValue).ToList() : TradeSearchModels.OrderByDescending(tsm => tsm.Ask).ToList();
                        break;
                    case "AskVolume":
                        TradeSearchModels = bAscending ? TradeSearchModels.OrderBy(tsm => tsm.AskVolume != null ? tsm.AskVolume : int.MaxValue).ToList() : TradeSearchModels.OrderByDescending(tsm => tsm.AskVolume).ToList();
                        break;
                    case "ProfitPerUnit":
                        TradeSearchModels = bAscending ? TradeSearchModels.OrderBy(tsm => tsm.ProfitPerUnit != null ? tsm.ProfitPerUnit : double.MaxValue).ToList() : TradeSearchModels.OrderByDescending(tsm => tsm.ProfitPerUnit).ToList();
                        break;
                    case "Profit":
                        TradeSearchModels = bAscending ? TradeSearchModels.OrderBy(tsm => tsm.Profit != null ? tsm.Profit : double.MaxValue).ToList() : TradeSearchModels.OrderByDescending(tsm => tsm.Profit).ToList();
                        break;
                    case "ProfitPer500":
                        TradeSearchModels = bAscending ? TradeSearchModels.OrderBy(tsm => tsm.PayoutPricePer500 != null ? tsm.PayoutPricePer500 : double.MaxValue).ToList() : TradeSearchModels.OrderByDescending(tsm => tsm.PayoutPricePer500).ToList();
                        break;
                }
            }
        }

        private void SortOriginOrderData(MatSortChangedEvent sort)
        {
            if (sort != null && sort.Direction != MatSortDirection.None && !String.IsNullOrWhiteSpace(sort.SortId))
            {
                bool bAscending = (sort.Direction == MatSortDirection.Asc);
                switch (sort.SortId)
                {
                    case "CompanyName":
                        OriginOrderModelsLists = bAscending ? OriginOrderModelsLists.OrderBy(om => om.CompanyName).ToList() : OriginOrderModelsLists.OrderByDescending(om => om.CompanyName).ToList();
                        break;
                    case "ItemCount":
                        OriginOrderModelsLists = bAscending ? OriginOrderModelsLists.OrderBy(om => om.ItemCount).ToList() : OriginOrderModelsLists.OrderByDescending(om => om.ItemCount).ToList();
                        break;
                    case "ItemCost":
                        OriginOrderModelsLists = bAscending ? OriginOrderModelsLists.OrderBy(om => om.ItemCost).ToList() : OriginOrderModelsLists.OrderByDescending(om => om.ItemCost).ToList();
                        break;
                }
            }
        }

        private void SortDestinationOrderData(MatSortChangedEvent sort)
        {
            if (sort != null && sort.Direction != MatSortDirection.None && !String.IsNullOrWhiteSpace(sort.SortId))
            {
                bool bAscending = (sort.Direction == MatSortDirection.Asc);
                switch (sort.SortId)
                {
                    case "CompanyName":
                        DestinationOrderModelsList = bAscending ? DestinationOrderModelsList.OrderBy(om => om.CompanyName).ToList() : DestinationOrderModelsList.OrderByDescending(om => om.CompanyName).ToList();
                        break;
                    case "ItemCount":
                        DestinationOrderModelsList = bAscending ? DestinationOrderModelsList.OrderBy(om => om.ItemCount).ToList() : DestinationOrderModelsList.OrderByDescending(om => om.ItemCount).ToList();
                        break;
                    case "ItemCost":
                        DestinationOrderModelsList = bAscending ? DestinationOrderModelsList.OrderBy(om => om.ItemCost).ToList() : DestinationOrderModelsList.OrderByDescending(om => om.ItemCost).ToList();
                        break;
                }
            }
        }

        private void OnExchangeSearchStringChanged(string newValue)
        {
            ExchangeSearchString = newValue;
            StateHasChanged();
        }

        private async void OnRowClick(object item)
        {
            if (item == null)
                return;

            Console.WriteLine("Clicked: " + item.ToString());
            if (item is not TradeFinderModel i)
                return;

            var l = await TradeFinderOrderModel.GetTradeFinderOrderModels($"{i.MaterialTicker}.{i.OriginExchangeCode}", $"{i.MaterialTicker}.{i.DestinationExchangeCode}");
            OriginOrderModelsLists = l[0];
            DestinationOrderModelsList = l[1];
            OriginExchangeTicker = $"{i.MaterialTicker}.{i.OriginExchangeCode}";
            DestinationExchangeTicker = $"{i.MaterialTicker}.{i.DestinationExchangeCode}";
            StateHasChanged();
        }
    }
}