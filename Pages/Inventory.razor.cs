using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

using FIOWebServer.Models;
using FIOWebServer.JsonPayloads;

using MatBlazor;
using Microsoft.AspNetCore.Components;
using CurrieTechnologies.Razor.Clipboard;

namespace FIOWebServer.Pages
{
    public partial class Inventory : ComponentBase, IDisposable
    {
        private string ActiveUserName = null;
        private List<InventoryModel> allInventory = null;
        private List<InventoryModel> filteredInventory = null;

        PermissionAllowance RequiredAllowances = new PermissionAllowance()
        {
            StorageData = true,
            FlightData = true,
            BuildingData = true
        };

        protected override async Task OnInitializedAsync()
        {
            GlobalAppState.OnChange += StateHasChanged;

            NavManager.TryGetQueryString<string>("UserName", out ActiveUserName);
            
            if (await GlobalAppState.LS_ContainsKey("InventoryShowEmptyStorage"))
            {
                _ShowEmptyStorage = await GlobalAppState.LSGet_Generic<bool>("InventoryShowEmptyStorage");
            }

            if (await GlobalAppState.LS_ContainsKey("InventoryShowShipFuelTanks"))
            {
                _ShowShipFuelTanks = await GlobalAppState.LSGet_Generic<bool>("InventoryShowShipFuelTanks");
            }
        }

        protected override async Task OnAfterRenderAsync(bool firstRender)
        {
            if (!firstRender && GlobalAppState.State == LoadState.Authenticated && allInventory == null)
            {
                if (ActiveUserName == null)
                {
                    ActiveUserName = await GlobalAppState.GetUserName();
                }

                await Refresh();
            }
        }

        private bool ShowShipFuelTanks
        {
            get
            {
                return _ShowShipFuelTanks;
            }
            set
            {
                if (_ShowShipFuelTanks != value)
                {
                    _ShowShipFuelTanks = value;
                    ApplyFilters();
                    _ = GlobalAppState.LSSet_Generic<bool>("InventoryShowShipFuelTanks", _ShowShipFuelTanks);
                }
            }
        }
        private bool _ShowShipFuelTanks = false;

        private bool ShowEmptyStorage
        {
            get
            {
                return _ShowEmptyStorage;
            }
            set
            {
                if (_ShowEmptyStorage != value)
                {
                    _ShowEmptyStorage = value;
                    ApplyFilters();
                    _ = GlobalAppState.LSSet_Generic<bool>("InventoryShowEmptyStorage", _ShowEmptyStorage);
                }
            }
        }
        private bool _ShowEmptyStorage = true;

        private void ApplyFilters()
        {
            filteredInventory = new List<InventoryModel>();
            allInventory.ForEach((item) => filteredInventory.Add((InventoryModel)item.Clone()));
            if (!ShowEmptyStorage)
            {
                filteredInventory.RemoveAll(im => im.Children.Count == 0);
            }

            if (!ShowShipFuelTanks)
            {
                filteredInventory.RemoveAll(im => im.StorageType == "Ship - FTL" || im.StorageType == "Ship - STL");
            }

            // Sort by material name
            foreach(var inventoryGroup in filteredInventory)
            {
                inventoryGroup.Children = inventoryGroup.Children.OrderBy(c => c.MaterialName).ToList();
            }

            StateHasChanged();
        }

        private List<SitesPayload> sites = null;
        private List<WarehousePayload> warehouses = null;
        private List<Ship> ships = null;
        private List<StoragePayload> storages = null;

        //private List<string> UsersAllowedToView = null;

        private int BaseCount = 0;
        private int WarehouseCount = 0;
        private int ShipCount = 0;

        private int ProgressCount = 0;
        private int TotalProgress = 5;
        private bool InRefresh = false;
        private async Task Refresh()
        {
            if (!InRefresh)
            {
                InRefresh = true;
                allInventory = null;
                filteredInventory = null;

                BaseCount = 0;
                WarehouseCount = 0;
                ShipCount = 0;


                ProgressCount = 0;
                StateHasChanged();

                var sitesRequest = new Web.Request(HttpMethod.Get, $"/sites/{ActiveUserName}", await GlobalAppState.GetAuthToken());
                sites = await sitesRequest.GetResponseAsync<List<SitesPayload>>();
                ProgressCount++;
                StateHasChanged();

                var warehousesRequest = new Web.Request(HttpMethod.Get, $"/sites/warehouses/{ActiveUserName}", await GlobalAppState.GetAuthToken());
                warehouses = await warehousesRequest.GetResponseAsync<List<WarehousePayload>>();
                ProgressCount++;
                StateHasChanged();

                var shipShipsRequest = new Web.Request(HttpMethod.Get, $"/ship/ships/{ActiveUserName}", await GlobalAppState.GetAuthToken());
                ships = await shipShipsRequest.GetResponseAsync<List<Ship>>();
                ProgressCount++;
                StateHasChanged();

                var storagesRequest = new Web.Request(HttpMethod.Get, $"/storage/{ActiveUserName}", await GlobalAppState.GetAuthToken());
                storages = await storagesRequest.GetResponseAsync<List<StoragePayload>>();
                ProgressCount++;
                StateHasChanged();

                var newInventoryModel = new List<InventoryModel>();
                if (storages != null)
                {
                    foreach (var storage in storages)
                    {
                        InventoryModel model = null;

                        if (storage.Type == "STORE") // Planet base
                        {
                            if (sites != null)
                            {
                                var site = sites.Where(s => s.SiteId == storage.AddressableId).FirstOrDefault();
                                if (site != null)
                                {
                                    model = new InventoryModel();
                                    model.LocationIdentifier = site.PlanetIdentifier;
                                    model.LocationName = site.PlanetName;
                                    model.StorageType = "Planet";
                                    if (!String.IsNullOrWhiteSpace(storage.StorageId) && storage.StorageId.Length >= 8)
                                    {
                                        model.StorageIdCommand = $"INV {storage.StorageId.Substring(0, 8)}";
                                    }
                                    BaseCount++;
                                }
                            }
                        }
                        else if (storage.Type == "WAREHOUSE_STORE") // Planet warehouse
                        {
                            if (warehouses != null)
                            {
                                var warehouse = warehouses.Where(w => w.WarehouseId.StartsWith(storage.AddressableId)).FirstOrDefault();
                                if (warehouse != null)
                                {
                                    model = new InventoryModel();
                                    model.LocationIdentifier = warehouse.LocationNaturalId;
                                    model.LocationName = warehouse.LocationName;
                                    model.StorageType = "Warehouse";
                                    if (!String.IsNullOrWhiteSpace(storage.StorageId) && storage.StorageId.Length >= 8)
                                    {
                                        model.StorageIdCommand = $"INV {storage.StorageId.Substring(0, 8)}";
                                    }
                                    WarehouseCount++;
                                }
                            }
                        }
                        else // SHIP_STORE, STL_FUEL_STORE, FTL_FUEL_STORE
                        {
                            if (ships != null)
                            {
                                var ship = ships.Where(s => s.ShipId == storage.AddressableId).FirstOrDefault();
                                if (ship != null)
                                {
                                    model = new InventoryModel();
                                    model.LocationIdentifier = ship.Registration;
                                    model.LocationName = ship.Name;
                                    if (storage.Type == "FTL_FUEL_STORE" && storage.StorageId == ship.FtlFuelStoreId)
                                    {
                                        model.StorageType = "Ship - FTL";
                                        model.StorageIdCommand = $"SHPF {ship.Registration}";
                                    }
                                    else if (storage.Type == "STL_FUEL_STORE" && storage.StorageId == ship.StlFuelStoreId)
                                    {
                                        model.StorageType = "Ship - STL";
                                        model.StorageIdCommand = $"SHPF {ship.Registration}";
                                    }
                                    else if (storage.Type == "SHIP_STORE" && storage.StorageId == ship.StoreId)
                                    {
                                        model.StorageType = "Ship";
                                        model.StorageIdCommand = $"SHPI {ship.Registration}";
                                        ShipCount++;
                                    }

                                    if (model.StorageType == "")
                                    {
                                        model = null;
                                    }
                                }
                            }
                        }

                        if (model != null)
                        {
                            foreach (var item in storage.StorageItems)
                            {
                                var subModel = new InventoryModel();
                                subModel.MaterialName = item.MaterialTicker;
                                subModel.Units = item.MaterialAmount;
                                model.Children.Add(subModel);
                            }

                            model.LastUpdate = storage.Timestamp;

                            newInventoryModel.Add(model);
                        }
                    }
                }

                // Sort by location name
                newInventoryModel = newInventoryModel.OrderBy(m => m.LocationDisplay).ToList();

                allInventory = newInventoryModel;

                ApplyFilters();

                ProgressCount++;
                StateHasChanged();
                InRefresh = false;
            }
        }

        public void Dispose()
        {
            GlobalAppState.OnChange -= StateHasChanged;
        }

        private string SearchString
        {
            get
            {
                return _SearchString;
            }
            set
            {
                _SearchString = value;
                Filter();
            }
        }
        private string _SearchString = "";

        private void Filter()
        {
            string UpperSearchString = "";
            if (SearchString != null)
            {
                UpperSearchString = SearchString.ToUpper();
            }

            filteredInventory = new List<InventoryModel>();
            allInventory.ForEach((item) => filteredInventory.Add((InventoryModel)item.Clone()));

            if (UpperSearchString.Length > 0)
            {
                for (int i = filteredInventory.Count - 1; i >= 0; --i)
                {
                    filteredInventory[i].Children.RemoveAll(c => c.MaterialName == null || c.MaterialName.ToUpper() != UpperSearchString);

                    if (filteredInventory[i].Children.Count == 0)
                    {
                        filteredInventory.RemoveAt(i);
                    }
                }
            }

            StateHasChanged();
        }

        private async Task CopyBufferCommandToClipboard(string BufferCommand)
        {
            if (!String.IsNullOrWhiteSpace(BufferCommand))
            {
                try
                {
                    await Clipboard.WriteTextAsync(BufferCommand);
                    Toaster.Add("Copied storage buffer command to clipboard.", MatToastType.Info, "Copied");
                }
                catch
                {
                    Toaster.Add("Browser failed to allow clipboard copying.", MatToastType.Danger, "Clipboard Failure");
                }
            }
        }
    }
}