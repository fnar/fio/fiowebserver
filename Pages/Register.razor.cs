using System.Net;

using MatBlazor;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.Components;

namespace FIOWebServer.Pages
{
    public partial class Register : ComponentBase
    {
        private string UserName = "";

        private string Password1 = "";
        private string Password2 = "";

        private string RegistrationGuid = "";

        private bool bLoading = true;
        protected override void OnInitialized()
        {
            NavManager.TryGetQueryString<string>("UserName", out UserName);
            NavManager.TryGetQueryString<string>("RegistrationGuid", out RegistrationGuid);
            if (String.IsNullOrWhiteSpace(UserName) || String.IsNullOrWhiteSpace(RegistrationGuid))
            {
                NavManager.NavigateTo("/");
            }

            bLoading = false;
        }

        private async Task CreateAccount(MouseEventArgs e)
        {
            if (Password1 != Password2)
            {
                Toaster.Add("Passwords do not match", MatToastType.Danger, "Match Failure");
                return;
            }

            if (Password1.Length < 4)
            {
                Toaster.Add("Passwords must be at least 5 characters in length", MatToastType.Danger, "Length Failure");
                return;
            }

            var registrationPayload = new JsonPayloads.PostRegistration();
            registrationPayload.UserName = UserName;
            registrationPayload.RegistrationGuid = RegistrationGuid;
            registrationPayload.Password = Password1;

            var createAccountRequest = new Web.Request(HttpMethod.Post, "/auth/register", null, JsonConvert.SerializeObject(registrationPayload));
            await createAccountRequest.GetResultNoResponse();
            if (createAccountRequest.StatusCode == HttpStatusCode.OK)
            {
                Toaster.Add("Success! Redirecting To Setup...", MatToastType.Success, "Redirecting...");
                await Task.Delay(1500);
                NavManager.NavigateTo("/setup");
            }
            else
            {
                Toaster.Add("Failed to create account.  You may have waited too long to register.", MatToastType.Danger, "Account Failure");
            }
        }
    }
}
