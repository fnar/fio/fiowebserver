using System.Globalization;

using MatBlazor;

using ChartJs.Blazor;
using ChartJs.Blazor.Common;
using ChartJs.Blazor.Common.Axes;
using ChartJs.Blazor.Common.Axes.Ticks;
using ChartJs.Blazor.Common.Enums;
using ChartJs.Blazor.Common.Time;
using ChartJs.Blazor.LineChart;
using ChartJs.Blazor.Util;
using Microsoft.AspNetCore.Components;

namespace FIOWebServer.Pages
{
    public partial class PriceIndices : ComponentBase, IDisposable
    {
        class PriceIndicesState
        {
            public double NumberOfDays { get; set; } = 30.0;
            public bool UpdateLoading { get; set; } = true;
            public bool average { get; set; } = false;
        }

        public class PriceIndexModel
        {
            public DateTime TimeStamp { get; set; }

            public string PriceIndexLabel { get; set; }
            public double PriceIndexValue { get; set; }
        }

        private MatTheme blackTheme = new MatTheme()
        {
            Primary = "black",
            Secondary = "black"
        };

        PriceIndicesState state = new PriceIndicesState();

        public LineConfig consumablesConfig;
        public Chart consumablesChart;

        public LineConfig buildingConfig;
        public Chart buildingChart;

        private bool HideCharts { get; set; } = true;

        public DateTime NumberDaysAgo = DateTime.UtcNow.AddDays(0);
        public DateTime NumberDaysAgoComp = DateTime.UtcNow.AddDays(0);

        public long start = 0;
        public long end = 0;

        public List<PriceIndexModel> statsResultInit = new List<PriceIndexModel>();

        private int CurrentProgress = 0;
        private void SetProgress(int Progress)
        {
            CurrentProgress = Progress;
            StateHasChanged();
        }

        protected override async Task OnInitializedAsync()
        {
            GlobalAppState.OnChange += StateHasChanged;

            var localFormat = CultureInfo.CurrentCulture.DateTimeFormat;
            string timeFormatSpecifier = $"{localFormat.ShortDatePattern} {localFormat.ShortTimePattern}";
            timeFormatSpecifier = timeFormatSpecifier.Replace("tt", "a"); // In C#, AM/PM is `tt`.  In chart.js (which is moment.js) it's `a`

            consumablesConfig = new LineConfig
            {
                Options = new LineOptions
                {
                    Animation = new Animation
                    {
                        Duration = 0,
                    },
                    Responsive = true,
                    Title = new OptionsTitle
                    {
                        Display = true,
                        Text = "Consumables"
                    },
                    Tooltips = new Tooltips
                    {
                        Mode = InteractionMode.Nearest,
                        Intersect = true
                    },
                    Hover = new Hover
                    {
                        Mode = InteractionMode.Nearest,
                        Intersect = true
                    },
                    Scales = new Scales
                    {
                        XAxes = new List<CartesianAxis>
                {
                        new TimeAxis
                        {
                            Ticks = new TimeTicks
                            {
                                SampleSize = 3,
                            },
                            Time = new TimeOptions
                            {
                                TooltipFormat = timeFormatSpecifier,
                            },
                            ScaleLabel = new ScaleLabel
                            {
                                LabelString = "Time"
                            }
                        }
                    },
                        YAxes = new List<CartesianAxis>
                {
                        new LinearCartesianAxis
                        {
                            Ticks = new LinearCartesianTicks
                            {
                                SuggestedMax = 100,
                            },
                            ScaleLabel = new ScaleLabel
                            {
                                LabelString = "Value"
                            }
                        }
                    }
                    }
                }
            };

            buildingConfig = new LineConfig
            {
                Options = new LineOptions
                {
                    Animation = new Animation
                    {
                        Duration = 0,
                    },
                    Responsive = true,
                    Title = new OptionsTitle
                    {
                        Display = true,
                        Text = "Buildings"
                    },
                    Tooltips = new Tooltips
                    {
                        Mode = InteractionMode.Nearest,
                        Intersect = true
                    },
                    Hover = new Hover
                    {
                        Mode = InteractionMode.Nearest,
                        Intersect = true
                    },
                    Scales = new Scales
                    {
                        XAxes = new List<CartesianAxis>
                {
                        new TimeAxis
                        {
                            Ticks = new TimeTicks
                            {
                                SampleSize = 3,
                            },
                            Time = new TimeOptions
                            {
                                TooltipFormat = timeFormatSpecifier,
                            },
                            ScaleLabel = new ScaleLabel
                            {
                                LabelString = "Time"
                            }
                        }
                    },
                        YAxes = new List<CartesianAxis>
                {
                        new LinearCartesianAxis
                        {
                            Ticks = new LinearCartesianTicks
                            {
                                SuggestedMax = 100,
                            },
                            ScaleLabel = new ScaleLabel
                            {
                                LabelString = "Value"
                            }
                        }
                    }
                    }
                }
            };

            NumberDaysAgo = DateTime.UtcNow.AddDays(-state.NumberOfDays);
            NumberDaysAgoComp = DateTime.UtcNow.AddDays(-state.NumberOfDays);

            start = NumberDaysAgo.ToUnixTime();
            end = Utils.GetCurrentEpochMs();

            var statsReq = new Web.Request(HttpMethod.Get, $"/stats/historical/priceindices/{start}/{end}");
            statsResultInit = await statsReq.GetResponseAsync<List<PriceIndexModel>>();
            if (statsResultInit == null) statsResultInit = new List<PriceIndexModel>();
            SetProgress(1);

            await UpdateAverage();
            SetProgress(2);
        }

        protected async Task UpdateAverage()
        {
            HideCharts = true;
            StateHasChanged();

            consumablesConfig.Data.Datasets.Clear();
            buildingConfig.Data.Datasets.Clear();

            NumberDaysAgo = DateTime.UtcNow.AddDays(-state.NumberOfDays);

            start = NumberDaysAgo.ToUnixTime();
            end = Utils.GetCurrentEpochMs();

            var statsResult = new List<PriceIndexModel>();
            if (NumberDaysAgo < NumberDaysAgoComp)
            {
                var statsReq = new Web.Request(HttpMethod.Get, $"/stats/historical/priceindices/{start}/{end}");
                statsResultInit = await statsReq.GetResponseAsync<List<PriceIndexModel>>();
                if (statsResultInit == null) statsResultInit = new List<PriceIndexModel>();
                NumberDaysAgoComp = NumberDaysAgo;
                foreach (PriceIndexModel item in statsResultInit)
                {
                    statsResult.Add(item);
                }
            }
            else
            {
                statsResult = statsResultInit.Where(sr => sr.TimeStamp > NumberDaysAgo).ToList();
            }

            if (statsResult.Count == 0)
            {
                state.UpdateLoading = false;
                HideCharts = false;
                Toaster.Add("No Data", MatToastType.Danger, "No data is available");
                return;
            }

            List<PriceIndexModel> consumables = statsResult.Where(sr => sr.PriceIndexLabel.StartsWith("Consumables-")).ToList();
            List<PriceIndexModel> consumablesICA = consumables.Where(sr => sr.PriceIndexLabel.EndsWith("ICA")).ToList();
            List<PriceIndexModel> consumablesCIS = consumables.Where(sr => sr.PriceIndexLabel.EndsWith("CIS")).ToList();
            List<PriceIndexModel> consumablesAIC = consumables.Where(sr => sr.PriceIndexLabel.EndsWith("AIC")).ToList();
            List<PriceIndexModel> consumablesNCC = consumables.Where(sr => sr.PriceIndexLabel.EndsWith("NCC")).ToList();

            foreach (PriceIndexModel element in consumablesICA) { element.PriceIndexValue = Utils.RoundToSignificantDigits(element.PriceIndexValue, 4); }
            foreach (PriceIndexModel element in consumablesCIS) { element.PriceIndexValue = Utils.RoundToSignificantDigits(element.PriceIndexValue, 4); }
            foreach (PriceIndexModel element in consumablesAIC) { element.PriceIndexValue = Utils.RoundToSignificantDigits(element.PriceIndexValue, 4); }
            foreach (PriceIndexModel element in consumablesNCC) { element.PriceIndexValue = Utils.RoundToSignificantDigits(element.PriceIndexValue, 4); }

            List<PriceIndexModel> consumablesICAave = nDayAverage(15, consumablesICA);
            List<PriceIndexModel> consumablesCISave = nDayAverage(15, consumablesCIS);
            List<PriceIndexModel> consumablesAICave = nDayAverage(15, consumablesAIC);
            List<PriceIndexModel> consumablesNCCave = nDayAverage(15, consumablesNCC);

            IDataset<TimePoint> results = new LineDataset<TimePoint>();
            results = CreateDataset(consumablesICA, ColorUtil.FromDrawingColor(System.Drawing.Color.Green));
            consumablesConfig.Data.Datasets.Add(results);
            results = CreateDataset(consumablesCIS, ColorUtil.FromDrawingColor(System.Drawing.Color.Blue));
            consumablesConfig.Data.Datasets.Add(results);
            results = CreateDataset(consumablesAIC, ColorUtil.FromDrawingColor(System.Drawing.Color.Red));
            consumablesConfig.Data.Datasets.Add(results);
            results = CreateDataset(consumablesNCC, ColorUtil.FromDrawingColor(System.Drawing.Color.Black));
            consumablesConfig.Data.Datasets.Add(results);

            if (state.average)
            {
                bool isAverage = true;
                results = CreateDataset(consumablesICAave, ColorUtil.FromDrawingColor(System.Drawing.Color.ForestGreen), isAverage);
                consumablesConfig.Data.Datasets.Add(results);
                results = CreateDataset(consumablesCISave, ColorUtil.FromDrawingColor(System.Drawing.Color.Navy), isAverage);
                consumablesConfig.Data.Datasets.Add(results);
                results = CreateDataset(consumablesAICave, ColorUtil.FromDrawingColor(System.Drawing.Color.IndianRed), isAverage);
                consumablesConfig.Data.Datasets.Add(results);
                results = CreateDataset(consumablesNCCave, ColorUtil.FromDrawingColor(System.Drawing.Color.DarkGray), isAverage);
                consumablesConfig.Data.Datasets.Add(results);
            }

            await consumablesChart.Update();

            List<PriceIndexModel> buildings = statsResult.Where(sr => sr.PriceIndexLabel.StartsWith("Building-")).ToList();
            List<PriceIndexModel> buildingsICA = buildings.Where(sr => sr.PriceIndexLabel.EndsWith("ICA")).ToList();
            List<PriceIndexModel> buildingsCIS = buildings.Where(sr => sr.PriceIndexLabel.EndsWith("CIS")).ToList();
            List<PriceIndexModel> buildingsAIC = buildings.Where(sr => sr.PriceIndexLabel.EndsWith("AIC")).ToList();
            List<PriceIndexModel> buildingsNCC = buildings.Where(sr => sr.PriceIndexLabel.EndsWith("NCC")).ToList();

            foreach (PriceIndexModel element in buildingsICA) { element.PriceIndexValue = Utils.RoundToSignificantDigits(element.PriceIndexValue, 4); }
            foreach (PriceIndexModel element in buildingsCIS) { element.PriceIndexValue = Utils.RoundToSignificantDigits(element.PriceIndexValue, 4); }
            foreach (PriceIndexModel element in buildingsAIC) { element.PriceIndexValue = Utils.RoundToSignificantDigits(element.PriceIndexValue, 4); }
            foreach (PriceIndexModel element in buildingsNCC) { element.PriceIndexValue = Utils.RoundToSignificantDigits(element.PriceIndexValue, 4); }

            List<PriceIndexModel> buildingsICAave = nDayAverage(15, buildingsICA);
            List<PriceIndexModel> buildingsCISave = nDayAverage(15, buildingsCIS);
            List<PriceIndexModel> buildingsAICave = nDayAverage(15, buildingsAIC);
            List<PriceIndexModel> buildingsNCCave = nDayAverage(15, buildingsNCC);

            results = CreateDataset(buildingsICA, ColorUtil.FromDrawingColor(System.Drawing.Color.Green));
            buildingConfig.Data.Datasets.Add(results);
            results = CreateDataset(buildingsCIS, ColorUtil.FromDrawingColor(System.Drawing.Color.Blue));
            buildingConfig.Data.Datasets.Add(results);
            results = CreateDataset(buildingsAIC, ColorUtil.FromDrawingColor(System.Drawing.Color.Red));
            buildingConfig.Data.Datasets.Add(results);
            results = CreateDataset(buildingsNCC, ColorUtil.FromDrawingColor(System.Drawing.Color.Black));
            buildingConfig.Data.Datasets.Add(results);

            if (state.average)
            {
                bool isAverage = true;
                results = CreateDataset(buildingsICAave, ColorUtil.FromDrawingColor(System.Drawing.Color.ForestGreen), isAverage);
                buildingConfig.Data.Datasets.Add(results);
                results = CreateDataset(buildingsCISave, ColorUtil.FromDrawingColor(System.Drawing.Color.Navy), isAverage);
                buildingConfig.Data.Datasets.Add(results);
                results = CreateDataset(buildingsAICave, ColorUtil.FromDrawingColor(System.Drawing.Color.IndianRed), isAverage);
                buildingConfig.Data.Datasets.Add(results);
                results = CreateDataset(buildingsNCCave, ColorUtil.FromDrawingColor(System.Drawing.Color.DarkGray), isAverage);
                buildingConfig.Data.Datasets.Add(results);
            }
            state.UpdateLoading = false;
            StateHasChanged();
            await buildingChart.Update();

            HideCharts = false;
            StateHasChanged();
        }

        private async Task UpdateClick()
        {
            state.UpdateLoading = true;
            StateHasChanged();
            await UpdateAverage();
        }

        public void Dispose()
        {
            GlobalAppState.OnChange -= StateHasChanged;
        }

        private static List<PriceIndexModel> nDayAverage(int frameSize, List<PriceIndexModel> values)
        {
            List<PriceIndexModel> averageModel = new List<PriceIndexModel>();
            foreach (var item in values)
            {
                averageModel.Add(new PriceIndexModel
                {
                    TimeStamp = item.TimeStamp,
                    PriceIndexLabel = item.PriceIndexLabel,
                    PriceIndexValue = item.PriceIndexValue
                });
            }
            List<double> data = values.Select(c => c.PriceIndexValue).ToList();
            double sum = 0;
            double[] avgPoints = new double[data.Count - frameSize + 1];
            for (int counter = 0; counter <= data.Count - frameSize; counter++)
            {
                int innerLoopCounter = 0;
                int index = counter;
                while (innerLoopCounter < frameSize)
                {
                    sum = sum + data[index];

                    innerLoopCounter += 1;

                    index += 1;

                }

                avgPoints[counter] = sum / frameSize;

                sum = 0;
            }
            for (int i = 0; i < avgPoints.Count(); ++i)
            {
                averageModel[i + frameSize - (int)(Math.Ceiling(frameSize / 2.0))].PriceIndexValue = Utils.RoundToSignificantDigits(avgPoints[i], 4);
            }
            for (int i = 0; i < (int)(Math.Floor(frameSize / 2.0)); ++i)
            {
                averageModel.RemoveAt(averageModel.Count - 1);
                averageModel.RemoveAt(1);
            }

            return averageModel;

        }

        public IDataset<TimePoint> CreateDataset(List<PriceIndexModel> set, String color, bool isAverage = false)
        {
            string lab = set[0].PriceIndexLabel;
            if (lab.Contains("ICA")) { lab = "IC1"; }
            else if (lab.Contains("CIS")) { lab = "CI1"; }
            else if (lab.Contains("AIC")) { lab = "AI1"; }
            else if (lab.Contains("NCC")) { lab = "NC1"; }
            if (isAverage)
            {
                IDataset<TimePoint> database = new LineDataset<TimePoint>(set.Select(c => new TimePoint(c.TimeStamp.ToLocalTime(), c.PriceIndexValue)))
                {
                    Label = lab + " SMA",
                    BackgroundColor = color,
                    BorderColor = color,
                    Fill = FillingMode.Disabled,
                    BorderWidth = 1,
                    PointStyle = PointStyle.Line,
                    PointRadius = 0
                };
                return database;
            }
            else
            {
                IDataset<TimePoint> database = new LineDataset<TimePoint>(set.Select(c => new TimePoint(c.TimeStamp.ToLocalTime(), c.PriceIndexValue)))
                {
                    Label = lab,
                    BackgroundColor = color,
                    BorderColor = color,
                    Fill = FillingMode.Disabled,
                    PointRadius = 0
                };
                return database;
            }

        }
    }
}