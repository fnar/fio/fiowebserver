using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using FIOWebServer.JsonPayloads;
using Microsoft.AspNetCore.Components;

namespace FIOWebServer.Pages
{
    public partial class Chat : ComponentBase, IDisposable
    {
        private int ProgressCount = 0;

        private string selectedChannel { get; set; } = "";
        private List<string> channelOptions = new List<string>();
        private List<ChatListItem> channelOptionsPayload = null!;
        private List<ChatMessage>? chatMessages = null;
        private string? chatMessageHtml = null;

        protected override async Task OnInitializedAsync()
        {
            if (NavManager.TryGetQueryString<string>("Channel", out var sc))
            {
                selectedChannel = sc;
            }

            var channelsReq = new Web.Request(HttpMethod.Get, "/chat/list");
            channelOptionsPayload = await channelsReq.GetResponseAsync<List<ChatListItem>>();
            if (channelOptionsPayload != null)
            {
                foreach (var cop in channelOptionsPayload)
                {
                    cop.DisplayName = cop.DisplayName.Replace("APEX Global Chat", "Global");
                    cop.DisplayName = cop.DisplayName.Replace("Official APEX Help Channel", "Help");
                    cop.DisplayName = cop.DisplayName.Replace(" Global Site Owners", "");
                }
                channelOptions = channelOptionsPayload.Select(cop => cop.DisplayName).ToList();
            }
            
            ProgressCount = 1;
            StateHasChanged();
        }

        private async Task OnSelectedChannelChanged(ChatListItem newValue)
        {
            selectedChannel = (newValue != null) ? newValue.DisplayName : "";
            chatMessageHtml = null;

            string? channelId = channelOptionsPayload.Where(cop => cop.DisplayName == selectedChannel).Select(cop => cop.ChannelId).FirstOrDefault();
            if (channelId != null)
            {
                NavManager.NavigateTo($"/chat?Channel={selectedChannel}");

                var channelMessagesReq = new Web.Request(HttpMethod.Get, $"/chat/display/{channelId}");
                chatMessages = await channelMessagesReq.GetResponseAsync<List<ChatMessage>>();

                StringBuilder sb = new StringBuilder();
                foreach (var chatMessage in chatMessages)
                {
                    DateTime messageTime = Utils.FromUnixTime(chatMessage.MessageTimestamp).ToLocalTime();
                    string messageTimeStr = messageTime.ToString("yyyy-MM-d HH:mm:ss");

                    switch (chatMessage.MessageType)
                    {
                        case "CHAT":
                            sb.AppendLine($"[{messageTimeStr}] {chatMessage.UserName}: {chatMessage.MessageText}<br/>");
                            break;
                        case "JOINED":
                            sb.AppendLine($"[{messageTimeStr}] {chatMessage.UserName} joined.<br/>");
                            break;
                        case "LEFT":
                            sb.AppendLine($"[{messageTimeStr}] {chatMessage.UserName} left.<br/>");
                            break;
                        case "DELETED":
                            sb.AppendLine($"[{messageTimeStr}] {chatMessage.UserName} deleted this message.<br/>");
                            break;
                    }
                }

                chatMessageHtml = sb.ToString();
            }
            else
            {
                selectedChannel = "";
            }

            StateHasChanged();
        }

        public void Dispose()
        {

        }
    }
}