using FIOWebServer.JsonPayloads;

using AntDesign;
using Microsoft.AspNetCore.Components;

namespace FIOWebServer.Pages
{
    public partial class MaterialViewer : ComponentBase
    {
        // Init time
        private List<ExchangePayload> Exchanges;
        private List<MaterialPayload> AllMaterials;

        // On-demand
        private List<ExchangeOrderPayload> ExchangeOrderData = null; //exchange/<TICKER>.<EXCHANGE_CODE>

        private List<TickerRecipeDescription> TickerRecipeProduction = new List<TickerRecipeDescription>();
        private List<TickerRecipeDescription> TickerRecipeWroughtProduct = new List<TickerRecipeDescription>();

        // UI stuff
        private string UISelectedTicker;
        private string SelectedTicker = null;
        List<string> MaterialTickers = new List<string>();

        private MaterialPayload SelectedMaterial;

        protected override async Task OnInitializedAsync()
        {
            SetProgress(0);
            AllMaterials = GlobalServerCache.Get<List<MaterialPayload>>("Material-AllMaterials")!;

            MaterialTickers = AllMaterials.Select(m => m.Ticker).ToList();
            MaterialTickers.Sort();

            SetProgress(1);
            Exchanges = GlobalServerCache.Get<List<ExchangePayload>>("ComexExchanges")!;

            SetProgress(2);

            NavManager.TryGetQueryString<string>("Ticker", out UISelectedTicker);
            if (UISelectedTicker != null)
            {
                UISelectedTicker = UISelectedTicker.ToUpper();
                await HandleNewTicker(UISelectedTicker);
            }

            SetProgress(3);
        }

        int CurrentProgress = 0;
        protected void SetProgress(int value)
        {
            CurrentProgress = value;
            StateHasChanged();
        }

        class TickerRecipeInputOutput
        {
            public int Amount { get; set; }
            public string Ticker { get; set; }
            public string MaterialCSSCategory { get; set; }

            public void PopulateMaterialCategoryName(string materialCategoryName)
            {
                switch (materialCategoryName)
                {
                    case "consumables (basic)":
                        MaterialCSSCategory = "consumables_basic";
                        break;
                    case "consumables (luxury)":
                        MaterialCSSCategory = "consumables_luxury";
                        break;
                    default:
                        if (materialCategoryName == null)
                        {
                            MaterialCSSCategory = "";
                        }

                        MaterialCSSCategory = materialCategoryName.Replace(" ", "_");
                        break;
                }
            }
        }

        class TickerRecipeDescription
        {
            public string BuildingTicker { get; set; }
            public string TimeInHours { get; set; }
            public List<TickerRecipeInputOutput> Inputs { get; set; } = new List<TickerRecipeInputOutput>();
            public List<TickerRecipeInputOutput> Outputs { get; set; } = new List<TickerRecipeInputOutput>();

            public TickerRecipeDescription(TickerRecipePayload payload, List<MaterialPayload> AllMaterials)
            {
                BuildingTicker = payload.BuildingTicker;
                var ts = new TimeSpan(0, 0, 0, 0, payload.DurationMs);
                if (ts.Minutes > 0)
                {
                    TimeInHours = $"{(int)ts.TotalHours}h {(int)ts.Minutes}m";
                }
                else
                {
                    TimeInHours = $"{(int)ts.TotalHours}h";
                }

                foreach(var input in payload.Inputs)
                {
                    var recipeInput = new TickerRecipeInputOutput();
                    recipeInput.Amount = input.Amount;
                    recipeInput.Ticker = input.CommodityTicker;

                    var materialCategoryName = AllMaterials.First(mat => mat.Ticker == recipeInput.Ticker).CategoryName;
                    recipeInput.PopulateMaterialCategoryName(materialCategoryName);

                    Inputs.Add(recipeInput);
                }

                foreach (var output in payload.Outputs)
                {
                    var recipeOutput = new TickerRecipeInputOutput();
                    recipeOutput.Amount = output.Amount;
                    recipeOutput.Ticker = output.CommodityTicker;

                    var materialCategoryName = AllMaterials.First(mat => mat.Ticker == recipeOutput.Ticker).CategoryName;
                    recipeOutput.PopulateMaterialCategoryName(materialCategoryName);

                    Outputs.Add(recipeOutput);
                }
            }
        }
        private bool bLoadingTicker = false;
        async Task HandleNewTicker(string ticker)
        {
            bLoadingTicker = true;
            StateHasChanged();

            if (!MaterialTickers.Contains(ticker))
            {
                return;
            }

            NavManager.NavigateTo($"/materialviewer?Ticker={UISelectedTicker}");

            SelectedTicker = ticker;
            SelectedMaterial = null;

            var request = new Web.Request(HttpMethod.Get, $"/recipes/{SelectedTicker}");
            var tickerRecipes = await request.GetResponseAsync<List<TickerRecipePayload>>();

            TickerRecipeProduction = new List<TickerRecipeDescription>();
            TickerRecipeWroughtProduct = new List<TickerRecipeDescription>();
            foreach (var tickerRecipe in tickerRecipes)
            {
                // "1xGRN 1xBEA 1xNUT = 10xRAT" => ["1xGRN 1xBEA 1xNUT ", " 10xRAT"]
                var splitRes = tickerRecipe.RecipeName.Split(new char[] { '=' }, StringSplitOptions.RemoveEmptyEntries);
                if (splitRes.Length == 2)
                {
                    var input = splitRes[0];
                    if (input.Contains($"x{SelectedTicker} "))
                    {
                        TickerRecipeWroughtProduct.Add(new TickerRecipeDescription(tickerRecipe, AllMaterials));
                    }
                    else
                    {
                        TickerRecipeProduction.Add(new TickerRecipeDescription(tickerRecipe, AllMaterials));
                    }
                }
            }

            TickerRecipeProduction = TickerRecipeProduction.OrderBy(trp => trp.BuildingTicker).ToList();
            TickerRecipeWroughtProduct = TickerRecipeWroughtProduct.OrderBy(trwp => trwp.BuildingTicker).ToList();

            ExchangeOrderData = null;
            var workingExchangeOrderData = new List<ExchangeOrderPayload>();
            foreach (var Exchange in Exchanges)
            {
                request = new Web.Request(HttpMethod.Get, $"/exchange/{SelectedTicker}.{Exchange.ExchangeCode}");
                var data = await request.GetResponseAsync<ExchangeOrderPayload>();

                data.BuyingOrders = data.BuyingOrders.OrderByDescending(bo => bo.ItemCost).ToList();
                data.SellingOrders = data.SellingOrders.OrderByDescending(so => so.ItemCost).ToList();

                workingExchangeOrderData.Add(data);
            }
            ExchangeOrderData = workingExchangeOrderData;

            SelectedMaterial = AllMaterials.FirstOrDefault(mat => mat.Ticker == SelectedTicker);

            bLoadingTicker = false;
            StateHasChanged();
        }

        async Task OnSelectionChange(AutoCompleteOption item)
        {
            await HandleNewTicker(item.Label);
        }
    }
}