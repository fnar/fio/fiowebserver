using FIOWebServer.Models;
using Microsoft.AspNetCore.Components;

namespace FIOWebServer.Pages
{
    public partial class ShipmentGPS : ComponentBase, IDisposable
    {
        private List<ShipmentGPSModel> Shipments = null;

        protected override async Task OnInitializedAsync()
        {
            GlobalAppState.OnChange += StateHasChanged;

            var shipmentsRequest = new Web.Request(HttpMethod.Get, "/contract/shipments", await GlobalAppState.GetAuthToken());
            Shipments = await shipmentsRequest.GetResponseAsync<List<ShipmentGPSModel>>();
        }

        public void Dispose()
        {
            GlobalAppState.OnChange -= StateHasChanged;
        }
    }
}