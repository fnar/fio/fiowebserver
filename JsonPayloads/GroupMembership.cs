﻿namespace FIOWebServer.JsonPayloads
{
    public class GroupMembership
    {
        public string GroupName { get; set; }
        public int GroupId { get; set; } = -1;

        public override string ToString()
        {
            return $"{GroupName}-{GroupId}";
        }

        public static GroupMembership Parse(string input)
        {
            GroupMembership result = new GroupMembership();
            if (input != null)
            {
                int groupId = -1;
                if (int.TryParse(input, out groupId))
                {
                    result.GroupId = groupId;
                }
                else
                {
                    var splitRes = input.Split("-");
                    if (splitRes.Length == 2 && int.TryParse(splitRes[1], out groupId))
                    {
                        result.GroupName = splitRes[0];
                        result.GroupId = groupId;
                    }
                }
            }

            return result;
        }
    }
}