using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace FIOWebServer.JsonPayloads
{
	class CreateGroupPayload
	{
		public string GroupId { get; set; }
		public string GroupName { get; set; }
		public List<string> GroupUsers { get; set; }
		public List<string> GroupAdmins { get; set; }
	}

	class CreateGroupResponse
	{
		public int GroupId { get; set; }
		public string GroupString { get; set; }
	}

	public class GroupPayload
	{
		public int GroupModelId { get; set; }

		public string GroupOwner { get; set; }

		public string GroupName { get; set; }

		public virtual List<GroupUserEntry> GroupUsers { get; set; } = new List<GroupUserEntry>();

		public virtual List<GroupModelAdmin> GroupAdmins { get; set; } = new List<GroupModelAdmin>();

		[JsonIgnore]
		public IEnumerable<string> GroupUserNames
		{
			get
			{
				var res = GroupUsers.Select(gu => gu.GroupUserName).OrderBy(gu => gu).ToList();
				return res;
			}
		}

		[JsonIgnore]
		public IEnumerable<string> GroupAdminUserNames
		{
			get
			{
				var res = GroupAdmins.Select(gu => gu.GroupAdminUserName).OrderBy(gaun => gaun).ToList();
				return res;
			}
		}
	}

	public class GroupUserEntry
	{
		public string GroupUserName { get; set; }

		public int GroupModelId { get; set; }
	}

	public class GroupModelAdmin
	{
		public string GroupAdminUserName { get; set; }
	}
}