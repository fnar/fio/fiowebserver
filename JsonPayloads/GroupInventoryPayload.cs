using System;
using System.Collections.Generic;

namespace FIOWebServer.JsonPayloads
{
    // A model of an individual inventory item of a player.
    public class InventoryItem
    {
        public string MaterialName { get; set; }

        public int Units { get; set; }

        public string MaterialType { get; set; }
    }


    public class StorageLocation
    {
        public string LocationIdentifier { get; set; }
        public string LocationName { get; set; }

        public string ShipLocationOrDestination { get; set; }
        public DateTime? LocationETA { get; set; }

        public DateTime? LocationETALocalTime
        {
            get
            {
                if (LocationETA != null)
                {
                    return ((DateTime)LocationETA).ToLocalTime();
                }

                return null;
            }
        }

        public string StorageType { get; set; }
        public string StorageIdCommand { get; set; }

        public List<InventoryItem> Items { get; set; } = new List<InventoryItem>();
    }

    public class GroupInventoryModel
    {
        public string UserName { get; set; }

        public List<StorageLocation> StorageLocations { get; set; } = new List<StorageLocation>();
    }
}