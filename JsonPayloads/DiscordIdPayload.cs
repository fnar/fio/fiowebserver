namespace FIOWebServer.JsonPayloads
{
	public class DiscordIdPayload
	{
		public string DiscordId { get; set; }
	}
}