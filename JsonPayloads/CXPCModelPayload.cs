using System;

namespace FIOWebServer.JsonPayloads
{
	public class CXPCModel
	{
		public long TimeEpochMs { get; set; }
		public float Open { get; set; }
		public float Close { get; set; }
		public float Volume { get; set; }
		public int Traded { get; set; }
	}
}
