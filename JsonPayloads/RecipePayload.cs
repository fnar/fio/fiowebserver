﻿using System.Collections.Generic;

namespace FIOWebServer.JsonPayloads
{
    public class RecipePayload
    {
        public string BuildingTicker { get; set; }
        public string RecipeName { get; set; }
        public List<RecipeInput> Inputs { get; set; }
        public List<RecipeOutput> Outputs { get; set; }
        public int TimeMs { get; set; }
    }

    public class RecipeInput
    {
        public string Ticker { get; set; }
        public int Amount { get; set; }
    }

    public class RecipeOutput
    {
        public string Ticker { get; set; }
        public int Amount { get; set; }
    }
}
