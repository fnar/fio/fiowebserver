namespace FIOWebServer.JsonPayloads
{
    public class ChatListItem
    {
        public string DisplayName { get; set; }
        public string ChannelId { get; set; }
    }

    public class ChatMessage
    {
        public string MessageType { get; set; }
        public string SenderId { get; set; }
        public string UserName { get; set; }
        public string MessageId { get; set; }
        public string MessageText { get; set; }
        public long MessageTimestamp { get; set; }
        public bool MessageDeleted { get; set; }
        public string UserNameSubmitted { get; set; }
        public DateTime Timestamp { get; set; }
    }
}