using System.Collections.Generic;

namespace FIOWebServer.JsonPayloads
{
    public class ExchangeOrderPayload
    {
        public List<OrderPayload> BuyingOrders { get; set; }
        public List<OrderPayload> SellingOrders { get; set; }
        public string MaterialName { get; set; }
        public string MaterialTicker { get; set; }
        public string MaterialId { get; set; }
        public string ExchangeName { get; set; }
        public string ExchangeCode { get; set; }
        public string Currency { get; set; }
        public double? Previous { get; set; }
        public double? Price { get; set; }
        public double? PriceTimeEpochMs { get; set; }
        public double? High { get; set; }
        public double? AllTimeHigh { get; set; }
        public double? Low { get; set; }
        public double? AllTimeLow { get; set; }
        public double? Ask { get; set; }
        public int? AskCount { get; set; }
        public double? Bid { get; set; }
        public int? BidCount { get; set; }
        public int? Supply { get; set; }
        public int? Demand { get; set; }
        public int? Traded { get; set; }
        public double? VolumeAmount { get; set; }
        public double? PriceAverage { get; set; }
        public double? NarrowPriceBandLow { get; set; }
        public double? NarrowPriceBandHigh { get; set; }
        public double? WidePriceBandLow { get; set; }
        public double? WidePriceBandHigh { get; set; }
        public double? MMBuy { get; set; }
        public double? MMSell { get; set; }
        public string UserNameSubmitted { get; set; }
        public string Timestamp { get; set; }

        public double? Spread
        {
            get
            {
                if (Ask != null && Bid != null)
                {
                    return (double)Ask - (double)Bid;
                }

                return null;
            }
        }
    }

    public class OrderPayload
    {
        public string CompanyId { get; set; }
        public string CompanyName { get; set; }
        public string CompanyCode { get; set; }
        public int? ItemCount { get; set; }
        public double? ItemCost { get; set; }
    }
}