using System;

namespace FIOWebServer.JsonPayloads
{
    public class MaterialPayload
    {
        public string MaterialId { get; set; }
        public string CategoryName { get; set; }
        public string CategoryId { get; set; }
        public string Name { get; set; }
        public string Ticker { get; set; }
        public float Weight { get; set; }
        public float Volume { get; set; }
        public string UserNameSubmitted { get; set; }
        public DateTime Timestamp { get; set; }
    }
}
