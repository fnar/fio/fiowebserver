
using System;
namespace FIOWebServer.JsonPayloads
{
    public class CurrencyPayload
    {
        public string BrokerId { get; set; }

        public string BaseCurrencyCode { get; set; }
        public string BaseCurrencyName { get; set; }
        public int BaseCurrencyNumericCode { get; set; }

        public string QuoteCurrencyCode { get; set; }
        public string QuoteCurrencyName { get; set; }
        public int QuoteCurrencyNumericCode { get; set; }

        public decimal High { get; set; }
        public decimal Low { get; set; }
        public decimal Open { get; set; }
        public decimal Previous { get; set; }
        public decimal Traded { get; set; }
        public decimal Volume { get; set; }
        public long PriceUpdateEpochMs { get; set; }
        
        //Submission data
        public string UserNameSubmitted { get; set; }
        public DateTime Timestamp { get; set; }
    }
}
