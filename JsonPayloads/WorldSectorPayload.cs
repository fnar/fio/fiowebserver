using System;
using System.Collections.Generic;

namespace FIOWebServer.JsonPayloads
{
    public class WorldSector
    {
        public string? SectorId { get; set; }
        public string? Name { get; set; }

        public int HexQ { get; set; }
        public int HexR { get; set; }
        public int HexS { get; set; }

        public int Size { get; set; }

        public List<SubSector> SubSectors { get; set; } = new List<SubSector>();

        public string? UserNameSubmitted { get; set; }
        public DateTime Timestamp { get; set; }
    }

    public class SubSector
    {
        public string? SSId { get; set; }

        public List<SubSectorVertex> Vertices { get; set; } = new List<SubSectorVertex>();
    }

    public class SubSectorVertex
    {
        public SubSectorVertex()
        {

        }

        public SubSectorVertex(double x, double y, double z)
        {
            X = x;
            Y = y;
            Z = z;
        }

        public double X { get; set; }
        public double Y { get; set; }
        public double Z { get; set; }

        public override bool Equals(object? obj)
        {
            if (obj == null) return false;
            var ssv = obj as SubSectorVertex;
            if (ssv == null) return false;

            return (X == ssv.X && Y == ssv.Y && Z == ssv.Z);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(X, Y, Z);
        }

        public static SubSectorVertex operator +(SubSectorVertex lhs, SubSectorVertex rhs)
        {
            return new SubSectorVertex(lhs.X + rhs.X, lhs.Y + rhs.Y, lhs.Z);
        }

        public static SubSectorVertex operator -(SubSectorVertex lhs, SubSectorVertex rhs)
        {
            return new SubSectorVertex(lhs.X - rhs.X, lhs.Y - rhs.Y, lhs.Z);
        }

        public static SubSectorVertex operator *(SubSectorVertex lhs, SubSectorVertex rhs)
        {
            return new SubSectorVertex(lhs.X * rhs.X, lhs.Y * rhs.Y, lhs.Z);
        }

        public static SubSectorVertex operator *(SubSectorVertex lhs, double magnitude)
        {
            return new SubSectorVertex(lhs.X * magnitude, lhs.Y * magnitude, lhs.Z);
        }
    }
}