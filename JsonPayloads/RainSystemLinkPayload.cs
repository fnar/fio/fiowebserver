namespace FIOWebServer.JsonPayloads
{
    public class RainSystemLink
    {
        public string Left { get; set; }
        public string Right { get; set; }
    }
}