using System;

namespace FIOWebServer.JsonPayloads
{
    public class WarehousePayload
    {
        public string WarehouseId { get; set; }
        public string StoreId { get; set; }
        public int Units { get; set; }
        public float WeightCapacity { get; set; }
        public float VolumeCapacity { get; set; }
        public long NextPaymentTimestampEpochMs { get; set; }
        public float? FeeAmount { get; set; }
        public string FeeCurrency { get; set; }
        public string FeeCollectorId { get; set; }
        public string FeeCollectorName { get; set; }
        public string FeeCollectorCode { get; set; }
        public string LocationName { get; set; }
        public string LocationNaturalId { get; set; }
        public string UserNameSubmitted { get; set; }
        public DateTime Timestamp { get; set; }
    }
}
