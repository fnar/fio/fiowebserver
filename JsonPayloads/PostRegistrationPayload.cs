namespace FIOWebServer.JsonPayloads
{
    public class PostRegistration
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string RegistrationGuid { get; set; }
    }
}
