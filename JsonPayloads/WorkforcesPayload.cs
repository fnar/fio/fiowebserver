using System;

namespace FIOWebServer.JsonPayloads
{
    public class WorkforcePayload
    {
        public Workforce[] Workforces { get; set; }
        public string PlanetId { get; set; }
        public string PlanetNaturalId { get; set; }
        public string PlanetName { get; set; }
        public string SiteId { get; set; }
        public DateTime? LastWorkforceUpdateTime { get; set; }
        public string UserNameSubmitted { get; set; }
        public DateTime Timestamp { get; set; }
    }

    public class Workforce
    {
        public Workforceneed[] WorkforceNeeds { get; set; }
        public string WorkforceTypeName { get; set; }
        public int Population { get; set; }
        public int Reserve { get; set; }
        public int Capacity { get; set; }
        public int Required { get; set; }
        public float Satisfaction { get; set; }
    }

    public class Workforceneed
    {
        public string Category { get; set; }
        public bool Essential { get; set; }
        public string MaterialId { get; set; }
        public string MaterialName { get; set; }
        public string MaterialTicker { get; set; }
        public float Satisfaction { get; set; }
        public float UnitsPerInterval { get; set; }
        public float UnitsPerOneHundred { get; set; }
    }
}