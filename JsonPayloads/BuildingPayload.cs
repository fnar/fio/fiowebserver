using System;
using System.Collections.Generic;

namespace FIOWebServer.JsonPayloads
{
    public class BuildingPayload
    {
        public List<BuildingCost> BuildingCosts { get; set; }
        public List<Recipe> Recipes { get; set; }
        public string Name { get; set; }
        public string Ticker { get; set; }
        public string Expertise { get; set; }
        public int Pioneers { get; set; }
        public int Settlers { get; set; }
        public int Technicians { get; set; }
        public int Engineers { get; set; }
        public int Scientists { get; set; }
        public int AreaCost { get; set; }
        public string UserNameSubmitted { get; set; }
        public DateTime Timestamp { get; set; }
    }

    public class BuildingCost
    {
        public string CommodityName { get; set; }
        public string CommodityTicker { get; set; }
        public float Weight { get; set; }
        public float Volume { get; set; }
        public int Amount { get; set; }
    }

    public class Recipe
    {
        public List<Input> Inputs { get; set; }
        public List<Output> Outputs { get; set; }
        public int DurationMs { get; set; }
        public string RecipeName { get; set; }
    }

    public class Input
    {
        public string CommodityName { get; set; }
        public string CommodityTicker { get; set; }
        public float Weight { get; set; }
        public float Volume { get; set; }
        public int Amount { get; set; }
    }

    public class Output
    {
        public string CommodityName { get; set; }
        public string CommodityTicker { get; set; }
        public float Weight { get; set; }
        public float Volume { get; set; }
        public int Amount { get; set; }
    }

}