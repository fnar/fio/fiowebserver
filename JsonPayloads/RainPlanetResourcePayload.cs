namespace FIOWebServer.JsonPayloads
{
    public class RainPlanetResource
    {
        public string Key { get; set; }
        public string Planet { get; set; }
        public string Ticker { get; set; }
        public string Type { get; set; }
        public double Factor { get; set; }
    }
}