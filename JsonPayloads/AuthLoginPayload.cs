namespace FIOWebServer.JsonPayloads
{
    public class JsonAuthLoginPayload
    {
        public string AuthToken { get; set; }
        public System.DateTime Expiry { get; set; }
    }
}
