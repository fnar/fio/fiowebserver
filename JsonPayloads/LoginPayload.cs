namespace FIOWebServer.JsonPayloads
{
    public class JsonLoginPayload
    {
        public string? UserName { get; set; }
        public string? Password { get; set; }
    }
}