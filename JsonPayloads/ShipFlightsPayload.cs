﻿using System;
using System.Collections.Generic;

namespace FIOWebServer.JsonPayloads
{
    public class Flight
    {
        public List<FlightSegment> Segments { get; set; } = new List<FlightSegment>();
        public string FlightId { get; set; }
        public string ShipId { get; set; }
        public string Origin { get; set; }
        public string Destination { get; set; }
        public long DepartureTimeEpochMs { get; set; }
        public long ArrivalTimeEpochMs { get; set; }
        public int CurrentSegmentIndex { get; set; }
        public float StlDistance { get; set; }
        public float FtlDistance { get; set; }
        public bool IsAborted { get; set; }
        public string UserNameSubmitted { get; set; }
        public DateTime Timestamp { get; set; }
    }

    public class FlightSegment
    {
        public List<OriginLine> OriginLines { get; set; } = new List<OriginLine>();
        public List<DestinationLine> DestinationLines { get; set; } = new List<DestinationLine>();
        public string Type { get; set; }
        public long DepartureTimeEpochMs { get; set; }
        public long ArrivalTimeEpochMs { get; set; }
        public float? StlDistance { get; set; }
        public float? StlFuelConsumption { get; set; }
        public float? FtlDistance { get; set; }
        public float? FtlFuelConsumption { get; set; }
        public string Origin { get; set; }
        public string Destination { get; set; }
    }

    public class OriginLine
    {
        public string Type { get; set; }
        public string LineId { get; set; }
        public string LineNaturalId { get; set; }
        public string LineName { get; set; }
    }

    public class DestinationLine
    {
        public string Type { get; set; }
        public string LineId { get; set; }
        public string LineNaturalId { get; set; }
        public string LineName { get; set; }
    }
}
