using System.Collections.Generic;

namespace FIOWebServer.JsonPayloads
{
    public class PlanetSearchPayload
    {
        public List<string> Materials { get; set; } = new List<string>();
        public double ConcentrationThreshold { get; set; } = 0.0;

        public bool IncludeRocky { get; set; }
        public bool IncludeGaseous { get; set; }

        public bool IncludeLowGravity { get; set; }
        public bool IncludeHighGravity { get; set; }

        public bool IncludeLowPressure { get; set; }
        public bool IncludeHighPressure { get; set; }

        public bool IncludeLowTemperature { get; set; }
        public bool IncludeHighTemperature { get; set; }

        public bool MustBeFertile { get; set; }
        public bool MustHaveLocalMarket { get; set; }
        public bool MustHaveChamberOfCommerce { get; set; }
        public bool MustHaveWarehouse { get; set; }
        public bool MustHaveAdministrationCenter { get; set; }
        public bool MustHaveShipyard { get; set; }

        public List<string> DistanceChecks { get; set; } = new List<string>();
    }
}
