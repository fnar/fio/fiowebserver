using System.Collections.Generic;

namespace FIOWebServer.JsonPayloads
{
    public class PlanetBurnRateSettings
    {
        public string PlanetNaturalId { get; set; }
        public List<MaterialExclusion> MaterialExclusions { get; set; } = new List<MaterialExclusion>();
    }

    public class MaterialExclusion
    {
        public string MaterialTicker { get; set; }
    }
}
