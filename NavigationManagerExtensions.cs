using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.Primitives;
using Microsoft.AspNetCore.WebUtilities;

namespace FIOWebServer
{
    public static class NavigationManagerExtensions
    {
        public static bool TryGetQueryString<T>(this NavigationManager navManager, string key, out T value)
        {
            var uri = navManager.ToAbsoluteUri(navManager.Uri);

            if (QueryHelpers.ParseQuery(uri.Query).TryGetValue(key, out var valueFromQueryString))
            {
                if (typeof(T) == typeof(int) && int.TryParse(valueFromQueryString, out var valueAsInt))
                {
                    value = (T)(object)valueAsInt;
                    return true;
                }

                if (typeof(T) == typeof(string))
                {
                    value = (T)(object)valueFromQueryString.ToString();
                    return true;
                }

                if (typeof(T) == typeof(decimal) && decimal.TryParse(valueFromQueryString, out var valueAsDecimal))
                {
                    value = (T)(object)valueAsDecimal;
                    return true;
                }
            }

            value = default;
            return false;
        }

        public static bool TryGetQueryValues(this NavigationManager navManager, string key, out List<string> values)
        {
            values = new List<string>();

            var uri = navManager.ToAbsoluteUri(navManager.Uri);
            if (QueryHelpers.ParseQuery(uri.Query).TryGetValue(key, out var valueFromQueryString))
            {
                var resultSet = new HashSet<string>();
                var workingList = valueFromQueryString.ToList();
                foreach (var workingListItem in workingList)
                {
                    var splitItems = workingListItem.Split(new char[] {'+', ' '}, StringSplitOptions.RemoveEmptyEntries).ToList();
                    foreach (var splitItem in splitItems)
                    {
                        resultSet.Add(splitItem);
                    }
                }

                values = resultSet.ToList();
                return true;
            }

            return false;
        }

        public static void SetQueryValues(this NavigationManager navManager, string key, List<string> values)
        {
            bool bHaveValues = values?.Count > 0;

            var uri = navManager.ToAbsoluteUri(navManager.Uri);
            var parsedQuery = QueryHelpers.ParseQuery(uri.Query);
            if (parsedQuery.TryGetValue(key, out var _))
            {
                if (bHaveValues)
                {
                    // Modify the values
                    parsedQuery.Remove(key);
                    parsedQuery.Add(key, new StringValues(values.ToArray()));
                }
                else
                {
                    // Remove the key from the URL
                    parsedQuery.Remove(key);
                }
            }
            else
            {
                if (bHaveValues)
                {
                    parsedQuery.Add(key, new StringValues(values.ToArray()));
                }
            }

            var relativePathNoParams = navManager.GetRelativePathWithoutParams();

            if (parsedQuery.Count > 0)
            {
                var queryParams = new StringBuilder();
                foreach (var kvp in parsedQuery)
                {
                    queryParams.Append(kvp.Key);
                    queryParams.Append("=");
                    queryParams.Append(kvp.Value.ToString().Replace(",", "+").Replace(" ", "+"));
                    queryParams.Append("&");
                }
                queryParams.Remove(queryParams.Length - 1, 1);

                navManager.NavigateTo($"{relativePathNoParams}?{queryParams}");
            }
            else
            {
                navManager.NavigateTo($"{relativePathNoParams}");
            }
        }

        public static string GetRelativePathWithParams(this NavigationManager navManager)
        {
            return navManager.Uri.Replace(navManager.BaseUri, "");
        }

        public static string GetRelativePathWithoutParams(this NavigationManager navManager)
        {
            return navManager.Uri.Split("?", StringSplitOptions.RemoveEmptyEntries)[0].Replace(navManager.BaseUri, "");
        }
    }
}