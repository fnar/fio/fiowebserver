using MatBlazor;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;

namespace FIOWebServer.Shared
{
    public partial class NavMenu : ComponentBase, IDisposable
    {
        private bool collapseNavMenu = true;

        private string? NavMenuCssClass => collapseNavMenu ? "collapse" : null;

        private void ToggleNavMenu()
        {
            collapseNavMenu = !collapseNavMenu;
        }

        protected override void OnInitialized()
        {
            //Thread.Sleep(3000);
            GlobalAppState.OnChange += StateHasChanged;
            _ = GlobalAppState.TryAuthCached();
        }

        public void Dispose()
        {
            GlobalAppState.OnChange -= StateHasChanged;
        }

        string? loginInputUserName;
        string? loginInputPassword;
        bool loginRemember { get; set; }

        bool loginDlgVisible = false;

        private async Task OnLoginOk(MouseEventArgs e)
        {
            loginDlgVisible = false;
            await Task.Delay(500);
            await GlobalAppState.Authenticate(loginInputUserName, loginInputPassword, loginRemember);
            if (GlobalAppState.State == LoadState.Authenticated)
            {
                Toaster.Add("Logged in", MatToastType.Success, "Authentication Success");
            }
            else if (GlobalAppState.State == LoadState.NotAuthenticated && GlobalAppState.IsServerDown)
            {
                Toaster.Add("Failed to log in", MatToastType.Danger, "FIO Server is not responding");
            }
            else
            {
                Toaster.Add("Failed to log in", MatToastType.Danger, "Authentication Failure");
            }
        }

        private async Task OnLoginCancel(MouseEventArgs e)
        {
            loginDlgVisible = false;
            await Task.Delay(500);
        }

        private async Task OnLogout(MouseEventArgs e)
        {
            await GlobalAppState.LogOut();
        }
    }
}