﻿using FIOWebServer.JsonPayloads;
using FIOWebServer.Models;
using System.Collections.Concurrent;
using Newtonsoft.Json;
using Timer = System.Timers.Timer;
using System.Collections.Generic;
using System.Diagnostics;

namespace FIOWebServer
{
    public interface IGlobalServerCacheItem
    {
        public string GetKey();
    }

    public class GlobalServerCacheItem<T> : IGlobalServerCacheItem where T : class
    {
        private object _lock = new();
        private T _cacheItem = null!;

        private string _key;
        private Task<T?> _updateFunc;
        private Timer _timer;

        public GlobalServerCacheItem(string key, Func<Task<T?>> updateFunc, int refreshSeconds = 60)
        {
            _key = key;
            _updateFunc = updateFunc();
            _timer = new Timer()
            {
                Interval = TimeSpan.FromSeconds(refreshSeconds).TotalMilliseconds,
                AutoReset = true,
            };
            _timer.Elapsed += timer_Tick;
            ForceRefreshSynchronous();
        }

        public string GetKey()
        {
            return _key;
        }

        public void ForceRefreshSynchronous()
        {
            updateCacheItem(_updateFunc.GetAwaiter().GetResult());
        }

        public async void ForceRefresh()
        {
            updateCacheItem(await _updateFunc);
        }

        private void timer_Tick(object? sender, EventArgs e)
        {
            ForceRefresh();
        }

        private string CacheItemFileName
        {
            get
            {
                return $"Cache-{_key}.json";
            }
        }

        private void SaveCacheItemFile(T cacheItemValue)
        {
            File.WriteAllText(CacheItemFileName, JsonConvert.SerializeObject(cacheItemValue));
        }

        private T GetCacheItemFileValue()
        {
            T? cacheItemValue = null;

            if (!File.Exists(CacheItemFileName))
            {
                throw new InvalidOperationException($"File {CacheItemFileName} does not exist");
            }

            var FileContents = File.ReadAllText(CacheItemFileName);
            try
            {
                cacheItemValue = JsonConvert.DeserializeObject<T>(FileContents);
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Failed to deserialize", ex);
            }

            if (cacheItemValue == null)
            {
                throw new InvalidOperationException("CacheFile object is null");
            }

            return (T)cacheItemValue;
        }

        private void updateCacheItem(T? item)
        {
            if (item != null)
            {
                lock(_lock)
                {
                    _cacheItem = item;
                }

                SaveCacheItemFile(item);
            }
            else
            {
                bool bExistingCacheIsNull;
                lock(_lock)
                {
                    bExistingCacheIsNull = _cacheItem == null;
                }

                if (bExistingCacheIsNull)
                {
                    // Load from disk
                    T FileCacheItem = GetCacheItemFileValue();

                    updateCacheItem(FileCacheItem);
                }
            }
        }

        public T Value
        {
            get
            {
                lock(_lock)
                {
                    return _cacheItem;
                }
            }
        }
    }

    public static class GlobalServerCache
    {
        private static ConcurrentDictionary<string, IGlobalServerCacheItem> _cacheItemNameToCacheItem = new();

        static GlobalServerCache()
        {
            Register(new GlobalServerCacheItem<List<MaterialPayload>>(
                "Material-Minerals",
                async () =>
                {
                    var req = new Web.Request(HttpMethod.Get, "/material/category/minerals");
                    return await req.GetResponseAsync<List<MaterialPayload>>();
                },
                (int)TimeSpan.FromDays(1).TotalSeconds));

            Register(new GlobalServerCacheItem<List<MaterialPayload>>(
                "Material-Ores",
                async () =>
                {
                    var req = new Web.Request(HttpMethod.Get, "/material/category/ores");
                    return await req.GetResponseAsync<List<MaterialPayload>>();
                }, 
                (int)TimeSpan.FromDays(1).TotalSeconds));

            Register(new GlobalServerCacheItem<List<MaterialPayload>>(
                "Material-Gases",
                async () =>
                {
                    var req = new Web.Request(HttpMethod.Get, "/material/category/gases");
                    return await req.GetResponseAsync<List<MaterialPayload>>();
                }, 
                (int)TimeSpan.FromDays(1).TotalSeconds));

            Register(new GlobalServerCacheItem<List<MaterialPayload>>(
                "Material-Liquids",async () =>
                {
                    var req = new Web.Request(HttpMethod.Get, "/material/category/liquids");
                    return await req.GetResponseAsync<List<MaterialPayload>>();
                }, 
                (int)TimeSpan.FromDays(1).TotalSeconds));

            Register(new GlobalServerCacheItem<List<MaterialPayload>>(
                "Material-Basic",
                async () =>
                {
                    var req = new Web.Request(HttpMethod.Get, "/material/category/consumables (basic)");
                    return await req.GetResponseAsync<List<MaterialPayload>>();
                }, 
                (int)TimeSpan.FromDays(1).TotalSeconds));

            Register(new GlobalServerCacheItem<List<MaterialPayload>>(
                "Material-Luxury",
                async () =>
                {
                    var req = new Web.Request(HttpMethod.Get, "/material/category/consumables (luxury)");
                    return await req.GetResponseAsync<List<MaterialPayload>>();
                }, 
                (int)TimeSpan.FromDays(1).TotalSeconds));

            Register(new GlobalServerCacheItem<List<MaterialPayload>>(
                "Material-AllMaterials",
                async () =>
                {
                    var req = new Web.Request(HttpMethod.Get, "/material/allmaterials");
                    return await req.GetResponseAsync<List<MaterialPayload>>();
                }, 
                (int)TimeSpan.FromDays(1).TotalSeconds));

            Register(new GlobalServerCacheItem<List<ExchangePayload>>(
                "ComexExchanges",
                async () =>
                {
                    var req = new Web.Request(HttpMethod.Get, "/global/comexexchanges");
                    return await req.GetResponseAsync<List<ExchangePayload>>();
                }, 
                (int)TimeSpan.FromDays(1).TotalSeconds));

            Register(new GlobalServerCacheItem<List<PlanetSearchModel>>(
                "PlanetSearchFull",
                async () =>
                {
                    var Minerals = Get<List<MaterialPayload>>("Material-Minerals");
                    var Ores = Get<List<MaterialPayload>>("Material-Ores");
                    var Liquids = Get<List<MaterialPayload>>("Material-Liquids");
                    var Gases = Get<List<MaterialPayload>>("Material-Gases");
                    if (Minerals == null || Ores == null || Liquids == null || Gases == null)
                    {
                        return null;
                    }

                    var AllPlanetMaterials = Minerals.Union(Ores.Union(Liquids.Union(Gases))).ToList();

                    var ComexExchanges = Get<List<ExchangePayload>>("ComexExchanges");
                    if (ComexExchanges == null)
                        return null;

                    var ExchangeLocationNames = ComexExchanges.Select(ce => ce.LocationName).ToList();
                    return await PlanetSearchModel.GetAllPlanets(AllPlanetMaterials, ExchangeLocationNames);
                }, 
                (int)TimeSpan.FromMinutes(5).TotalSeconds));
        }

        public static void Init()
        {
            // Empty init call to force-instatiate the initial data
        }

        private static bool Register(IGlobalServerCacheItem CacheItem)
        {
            return _cacheItemNameToCacheItem.TryAdd(CacheItem.GetKey(), CacheItem);
        }

        public static T? Get<T>(string cacheItemName) where T : class
        {
            var CacheItem = Get(cacheItemName);
            var TypedCacheItem = CacheItem as GlobalServerCacheItem<T>;
            var Value = TypedCacheItem?.Value.Copy();
            return Value;
        }

        private static IGlobalServerCacheItem? Get(string cacheItemName)
        {
            if (_cacheItemNameToCacheItem.TryGetValue(cacheItemName, out var value))
            {
                return value;
            }

            return null;
        }
    }
}
