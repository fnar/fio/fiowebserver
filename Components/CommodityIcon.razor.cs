using Microsoft.AspNetCore.Components;
using System.Collections.ObjectModel;

namespace FIOWebServer.Components
{
    public partial class CommodityIcon : ComponentBase
    {
        [Parameter]
        public string MaterialTicker
        {
            get;
            set;
        } = "";

        [Parameter]
        public string Category
        {
            get;
            set;
        } = "gases";

        [Parameter]
        public IconSize Size
        {
            get;
            set;
        } = IconSize.SMALL;


        public enum IconSize
        {
            SMALL = 0,
            BIG = 1,
        }

        private Dictionary<int, string> IconSizeCssClassMap = new Dictionary<int, string>()
        { [(int)IconSize.SMALL] = "materialIconSmall", [(int)IconSize.BIG] = "materialIconBig", };

        //Maps payload category to css class
        private readonly static ReadOnlyDictionary<string, string> PayloadToCssDict = new ReadOnlyDictionary<string, string>(new Dictionary<string, string>()
        {
            ["agricultural products"] = "agriculturalProducts",
            ["alloys"] = "alloys",
            ["chemicals"] = "chemicals",
            ["construction materials"] = "constructionMaterials",
            ["construction parts"] = "constructionParts",
            ["construction prefabs"] = "  constructionPrefabs",
            ["consumables (basic)"] = "consumablesBasic",
            ["consumables (luxury)"] = "consumablesLuxury",
            ["drones"] = "drones",
            ["electronic devices"] = "electronicDevices",
            ["electronic parts"] = "electronicParts",
            ["electronic pieces"] = "electronicPieces",
            ["electronic systems"] = "electronicSystems",
            ["elements"] = "elements",
            ["energy systems"] = "energySystems",
            ["fuels"] = "fuels",
            ["gases"] = "gases",
            ["liquids"] = "liquids",
            ["medical equipment"] = "medicalEquipment",
            ["metals"] = "metals",
            ["minerals"] = "minerals",
            ["ores"] = "ores",
            ["plastics"] = "plastics",
            ["ship engines"] = "shipEngines",
            ["ship kits"] = "shipKits",
            ["ship parts"] = "shipParts",
            ["ship shields"] = "shipShields",
            ["software components"] = "softwareComponents",
            ["software systems"] = "softwareSystems",
            ["software tools"] = "softwareTools",
            ["textiles"] = "textiles",
            ["unit prefabs"] = "unitPrefabs",
            ["utility"] = "utility",
        });

        public static string MatPayloadToCss(string input, string defaultClass = "gases")
        {
            if (PayloadToCssDict.TryGetValue(input, out string? val))
                return val;
            return defaultClass;
        }
    }
}