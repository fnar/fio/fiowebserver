﻿using System;
using Microsoft.AspNetCore.Components;

using FIOWebServer.Models;

namespace FIOWebServer.Components.Burn
{
	public partial class LastUpdateNotify
	{
		[Parameter]
		public BurnModel? Context { get; set; }

		public MarkupString Reason
		{
			get
			{
				string reason = "";
				if (Context?.LastUpdate == null)
                {
                    reason += "Data was never retrieved for your workforce/storage. Open up a <code>BS</code> buffer and navigate to this base.";
                }
                else
                {
                    reason += (Context.LastUpdateCause == "WORKFORCE") ? "Workforce" : "Storage";
                    reason += $" data was last updated <b>{((DateTime)(Context.LastUpdate)).ToLocalTime()}</b>.<br />";
                    reason += "Open up a <code>BS</code> buffer and navigate to this base.";
                }

				return (MarkupString)reason;
			}
		}
	}
}