﻿using Microsoft.AspNetCore.Components;

namespace FIOWebServer.Components
{
	public partial class LoadingWrapper : ComponentBase, IDisposable
    {
        [Parameter]
		public RenderFragment ChildContent { get; set; } = null!;

        [Parameter]
		public bool RequiresAuth { get; set; } = false;

		[Parameter]
		public bool RequiresAdmin { get; set; } = false;

		[Parameter]
		public int ProgressCount { get; set; } = 0;

		[Parameter]
		public EventCallback<int> ProgressCountChanged { get; set; }

		[Parameter]
		public int TotalProgress { get; set; } = 0;

		[Parameter]
		public EventCallback<int> TotalProgressChanged { get; set; }

		protected override async Task OnInitializedAsync()
		{
			await Task.CompletedTask;
            GlobalAppState.OnChange += StateHasChanged;
        }

        public void Dispose()
        {
            GlobalAppState.OnChange -= StateHasChanged;
        }

        public bool IsLoaded
		{
			get
			{
				return TotalProgress == 0 || ProgressCount == TotalProgress;
			}
		}

		public string ProgressStr
		{
			get
			{
				if (IsLoaded)
				{
					return "100%";
				}
				else
				{
					if (TotalProgress > 1)
                    {
						double value = ((double)ProgressCount / TotalProgress) * 100.0;
						return Utils.RoundToSignificantDigitsStr(value, 2) + "%";
					}
					else
                    {
						return string.Empty;
                    }
				}
			}
		}
    }
}