﻿using Microsoft.AspNetCore.Components;

namespace FIOWebServer.Components
{
	public partial class UrlFilter : ComponentBase, IDisposable
	{
        [Parameter]
		public string Placeholder { get; set; } = "";

		[Parameter]
		public IEnumerable<string> SelectedOptions
		{
			get => _SelectedOptions; 
			set
			{
				if (!_SelectedOptions.SequenceEqual(value))
                {
					_SelectedOptions = value;
					_SelectedOptions = _SelectedOptions.Select(so => so.ToUpper());
					SelectedOptionsChanged.InvokeAsync(value);
				}
			}
		}
		private IEnumerable<string> _SelectedOptions = new List<string>();

        [Parameter]
		public EventCallback<IEnumerable<string>> SelectedOptionsChanged { get; set; }

		[Parameter]
		public IEnumerable<string> AllOptions { get; set; } = null!;

        [Parameter]
		public EventCallback FilterChanged { get; set; }

		[Parameter]
		public string URLQueryKey { get; set; } = null!;

		private async void InternalSelectionsChanged(IEnumerable<string> values)
        {
            NavManager.SetQueryValues(URLQueryKey, values.ToList());
			await FilterChanged.InvokeAsync();
		}

		protected override void OnInitialized()
        {
			GlobalAppState.OnChange += StateHasChanged;
		}

		public void Dispose()
		{
			GlobalAppState.OnChange -= StateHasChanged;
		}

		private bool bSetParameters = false;
		protected override void OnParametersSet()
        {
			if (!bSetParameters)
            {
				List<string> values;
				NavManager.TryGetQueryValues(URLQueryKey, out values);
				values = values.Select(v => v.ToUpper()).ToList();
				SelectedOptions = values.Intersect(AllOptions);
				NavManager.SetQueryValues(URLQueryKey, SelectedOptions.ToList());

				bSetParameters = true;
			}
		}
	}
}